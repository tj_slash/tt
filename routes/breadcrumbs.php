<?php
Breadcrumbs::register('home', function ($breadcrumbs) {
	$breadcrumbs->push('Главная', action('MainController@index'));
});

Breadcrumbs::register('shares', function ($breadcrumbs) {
	$breadcrumbs->parent('home');
	$breadcrumbs->push('Акции', action('SharesController@index'));
});
Breadcrumbs::register('search', function ($breadcrumbs) {
	$breadcrumbs->parent('home');
	$breadcrumbs->push('Поиск', action('MainController@search'));
});
Breadcrumbs::register('shares-view', function ($breadcrumbs, $share) {
	$breadcrumbs->parent('shares');
	$breadcrumbs->push($share->title, action('SharesController@view', $share->slug));
});

Breadcrumbs::register('brands', function ($breadcrumbs) {
	$breadcrumbs->parent('home');
	$breadcrumbs->push('Наши бренды', action('BrandsController@index'));
});

Breadcrumbs::register('contacs', function ($breadcrumbs) {
	$breadcrumbs->parent('home');
	$breadcrumbs->push('Наши контакты', action('ContactsController@index'));
});

Breadcrumbs::register('floors', function ($breadcrumbs) {
	$breadcrumbs->parent('home');
	$breadcrumbs->push('Этажи', action('MainController@floors'));
});

Breadcrumbs::register('page', function ($breadcrumbs, $page) {
	$breadcrumbs->parent('home');
	$breadcrumbs->push($page->title, action('PagesController@view', $page->slug));
});

Breadcrumbs::register('look-book', function ($breadcrumbs) {
	$breadcrumbs->parent('home');
	$breadcrumbs->push('Look Book', action('LookController@index'));
});

Breadcrumbs::register('news', function ($breadcrumbs) {
	$breadcrumbs->parent('home');
	$breadcrumbs->push('Новости и статьи', action('NewsController@index'));
});
Breadcrumbs::register('news-view', function ($breadcrumbs, $news) {
	$breadcrumbs->parent('news');
	$breadcrumbs->push($news->title, action('NewsController@index', $news->slug));
});

Breadcrumbs::register('photoreports', function ($breadcrumbs) {
	$breadcrumbs->parent('home');
	$breadcrumbs->push('Фоторепортажи', action('PhotoreportsController@index'));
});
Breadcrumbs::register('photoreports-view', function ($breadcrumbs, $photoreport) {
	$breadcrumbs->parent('photoreports');
	$breadcrumbs->push($photoreport->title, action('PhotoreportsController@index', $photoreport->slug));
});

Breadcrumbs::register('catalog', function ($breadcrumbs) {
	$breadcrumbs->parent('home');
	$breadcrumbs->push('Каталог', action('CatalogController@index', null, null));
});

Breadcrumbs::register('catalog-category', function ($breadcrumbs, $category) {
	$breadcrumbs->parent('catalog');
	foreach ($category->ancestors()->get() as $parent) {
		$breadcrumbs->push($parent->title, action('CatalogController@index', ['category_slug' => $parent->slug]));
	}
	$breadcrumbs->push($category->title, action('CatalogController@index', ['category_slug' => $category->slug]));
});

Breadcrumbs::register('catalog-brand', function ($breadcrumbs, $brand) {
	$breadcrumbs->parent('catalog');
	$breadcrumbs->push($brand->title, action('CatalogController@index', ['brand_slug' => $brand->slug]));
});

Breadcrumbs::register('catalog-product', function ($breadcrumbs, $product) {
	$breadcrumbs->parent('catalog-category', $product->category);
	$breadcrumbs->push($product->title, action('CatalogController@view', $product->slug));
});