<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index')->name('home');
Route::get('/floors', 'MainController@floors')->name('floors');
Route::get('/search', 'MainController@search')->name('search');
Route::get('/sitemap.xml', 'MainController@test')->name('test');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout.get');

// News
Route::group(['prefix' => '/news'], function () {
	Route::get('', 'NewsController@index')->name('news');
	Route::get('/list/get', 'NewsController@get')->name('news.get');
	Route::get('{slug}', 'NewsController@view')->name('news.view');
});

// Photoreports
Route::group(['prefix' => '/photoreports'], function () {
	Route::get('', 'PhotoreportsController@index')->name('photoreports');
	Route::get('/list/get', 'PhotoreportsController@get')->name('photoreports.get');
	Route::get('{slug}', 'PhotoreportsController@view')->name('photoreports.view');
});

// Pages
Route::group(['prefix' => '/pages'], function () {
	Route::get('{slug}', 'PagesController@view')->name('pages.view');
});

// Brands
Route::group(['prefix' => '/brands'], function () {
	Route::get('get', 'BrandsController@get')->name('brands.get');
	Route::get('', 'BrandsController@index')->name('brands');
	Route::get('{slug}', 'BrandsController@view')->name('brands.view');
});

// Catalog
Route::group(['prefix' => '/catalog'], function () {
	Route::get('search', 'CatalogController@search')->name('catalog.search');
	Route::get('look-book/{look_slug}', 'CatalogController@index')->name('catalog.look.view');
	Route::get('category/{category_slug}', 'CatalogController@index')->name('catalog.category.view');
	Route::get('brands/{brand_slug}', 'CatalogController@index')->name('catalog.brands.view');
	Route::get('newest', 'CatalogController@index')->name('catalog.newest.view');
	Route::get('{slug}', 'CatalogController@view')->name('catalog.view');
	Route::get('', 'CatalogController@index')->name('catalog');
});

// Shares
Route::group(['prefix' => '/shares'], function () {
	Route::get('', 'SharesController@index')->name('shares');
	Route::get('{slug}', 'SharesController@view')->name('shares.view');
});

// Contacts
Route::group(['prefix' => '/contacts'], function () {
	Route::get('', 'ContactsController@index')->name('contacts');
	Route::post('/send', 'ContactsController@send')->name('contacts.send');
});

// Subscribes
Route::group(['prefix' => '/subscribes'], function () {
	Route::post('/send', 'SubscribesController@send')->name('subscribes.send');
});

// Gifts
Route::group(['prefix' => '/gifts'], function () {
	Route::post('/send', 'GiftsController@send')->name('gifts.send');
});

// Look Book
Route::group(['prefix' => '/look-book'], function () {
	Route::get('{slug}', 'LookController@index')->name('look-book.view');
	Route::get('', 'LookController@index')->name('look-book');
});



/*

Route::get('/catalog', 'MainController@catalog')->name('catalog');
Route::get('/catalog/view', 'MainController@catalogView')->name('catalog-view');*/
Auth::routes();