<?php

namespace App\Http\Controllers;

use App\Http\Requests\FeedbackRequest;
use App\Models\Feedbacks;
use Session;

class ContactsController extends Controller
{
    /**
	 * Contacts page
	 *
	 * @return \Illuminate\Http\Response
	 */
    public function index()
	{
		return view('contacts/index');
	}

	public function send(FeedbackRequest $request)
	{
        $feedback = new Feedbacks($request->all());
        if (!$feedback->save()) {
	        Session::flash('flash_notification.message', 'Произошла ошибка');
	    	Session::flash('flash_notification.level', 'error');
            return redirect()->back();
        }
        Session::flash('flash_notification.message', 'Ваше сообщение успешно отправлено!');
    	Session::flash('flash_notification.level', 'success');
        return redirect()->back();
	}

	/**
	 * Widget banners on main page
	 *
	 * @return \Illuminate\Http\Response
	 */
    public static function widget()
	{
		return view('contacts/widget');
	}
}
