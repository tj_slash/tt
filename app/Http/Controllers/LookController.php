<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Looks;
use App\Models\Pages;

class LookController extends Controller
{
	/**
	 * Look Book
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$looks = Looks::sortCreated()->get();

		if ($request->slug) {
			$look = Looks::where('slug', $request->slug)->first();
		} else {
			$look = Looks::sortCreated()->first();
		}

		$page = Pages::where('slug', 'look-book')->first();

		return view('look/index', compact('look', 'looks', 'page'));
	}
}