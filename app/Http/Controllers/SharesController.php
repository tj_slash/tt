<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Shares;

class SharesController extends Controller
{
	/**
	 * All chares
	 * @return [type] [description]
	 */
    public function index()
	{
		$shares = Shares::sortCreated()->get();
		return view('shares/index', compact('shares'));
	}

	/**
	 * View share
	 * @param  [type] $slug [description]
	 * @return [type]       [description]
	 */
	public function view($slug)
	{
		$share = Shares::where('slug', $slug)->first();
		return view('shares/view', compact('share'));
	}

	/**
	 * Widget on main page
	 * @return [type] [description]
	 */
	public function widget()
	{
		$shares = Shares::sortCreated()->paginate(6);
		return view('home/shares', compact('shares'));
	}
}
