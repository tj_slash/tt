<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pages;

class PagesController extends Controller
{
    /**
	 * Pages view
	 *
	 * @return \Illuminate\Http\Response
	 */
    public function view($slug)
	{
		$page = Pages::where('slug', $slug)->first();

		return view('pages/view', compact('page'));
	}
}