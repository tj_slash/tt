<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Subscribes;
use Session;

class SubscribesController extends Controller
{
	public function send(Request $request)
	{
        $subscribe = new Subscribes($request->all());
        if (!$subscribe->save()) {
	        Session::flash('flash_notification.message', 'Произошла ошибка');
	    	Session::flash('flash_notification.level', 'error');
            return redirect()->back();
        }
        Session::flash('flash_notification.message', 'Вы успешно подписались на новинки');
    	Session::flash('flash_notification.level', 'success');
        return redirect()->back();
	}

	/**
	 * Widget banners on main page
	 *
	 * @return \Illuminate\Http\Response
	 */
    public static function widget()
	{
		return view('subscribes/widget');
	}
}
