<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Banners;

class BannersController extends Controller
{
	/**
	 * Widget banners on main page
	 *
	 * @return \Illuminate\Http\Response
	 */
    public static function widget()
	{
		$banners = Banners::limit(5)->sortOrdered()->get();
		return view('banners/widget', compact('banners'));
	}
}
