<?php

namespace App\Http\Controllers;

use function Clue\StreamFilter\fun;
use Illuminate\Http\Request;
use Jenssegers\Date\Date;

use App\Models\Photoreports;
use Carbon\Carbon;

class PhotoreportsController extends Controller
{
	/**
	 * News list
	 *
	 * @return \Illuminate\Http\Response
	 */
    public function index()
	{
		$lastPhotoreport = Photoreports::main()->sortCreated()->first();

		$photoreports = Photoreports::sortCreated()->paginate(6);

		return view('photoreports/index', compact('photoreports', 'lastPhotoreport'));
	}

	/**
	 * News view
	 *
	 * @return \Illuminate\Http\Response
	 */
    public function view($slug)
	{
		$photoreport = Photoreports::where('slug', $slug)->first();
		$smallImages = collect($photoreport->images)->map(function ($item, $key) {
		    return route('imagecache', ['photo_report_small', $item]);
        });

		return view('photoreports/view', compact('photoreport', 'smallImages'));
	}

	public function get(Request $request)
	{
		$last = false;
		if ($request->input('start') == 0) {

			$last = Photoreports::main();

			$last = $last->sortCreated()->limit(1)->get()->map(function($item, $key) {
				$item->url = $item->url;
				$item->big_image = route('imagecache', ['news_big', $item->big_image]);
				$item->date = Date::parse($item->created_at)->format('j F');
				return $item;
			});
		}


		$photoreports = Photoreports::sortCreated()->skip($request->input('start'))
			->take($request->input('limit'))
			->get()
			->map(function($item, $key) {
				$item->url = $item->url;
				$item->image = route('imagecache', ['medium', $item->image]);
				$item->date = Date::parse($item->created_at)->format('j F');
				return $item;
			});

		$count = Photoreports::count();

		if ($request->input('start') == 0 && count($last) > 0) {
			return response()->json([
				'last' => $last[0],
				'photoreports' => $photoreports,
				'count' => $count
			]);
		}

		return response()->json([
			'photoreports' => $photoreports,
			'count' => $count
		]);
	}
}
