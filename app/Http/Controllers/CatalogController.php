<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categories;
use App\Models\Brands;
use App\Models\Ads;
use App\Models\Colors;
use App\Models\Sizes;
use App\Models\Looks;
use App\Models\Variants;
use App\Models\Pages;

class CatalogController extends Controller
{
    /**
     * Catalog
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $category = $brand = $look = false;

        if (!empty($request->brand_slug)) {
            $brand = Brands::where('slug', $request->brand_slug)->first();
        }

        if (!empty($request->category_slug)) {
            $category = Categories::where('slug', $request->category_slug)->first();
        }

        if (!empty($request->look_slug)) {
            $look = Looks::where('slug', $request->look_slug)->first();
        }

        $sorting = Ads::SORT_DEFAULT;
        if ($request->route()->named('catalog.newest.view')) {
            $sorting = Ads::SORT_NEW;
        }

        $colors = Colors::select('title', 'id')->get()->toJson();
        $brands = Brands::select('title', 'id')->get()->toJson();
        $sizes = Sizes::select('title', 'id')->get()->toJson();

        return view('catalog/index', compact('products', 'count', 'category', 'brand', 'colors', 'brands', 'sizes', 'look', 'sorting'));
    }

    /**
     * View product
     *
     * @return \Illuminate\Http\Response
     */
    public function view($slug)
    {
        $product = Ads::where('slug', $slug)->limit(1)->get()->map(function ($item, $key) {
            if ($item->colors) {
                $item->colors = Colors::select('title', 'id')->whereIn('id', $item->colors)->get()->toArray();
            }
            if ($item->sizes) {
                $item->sizes = Sizes::select('title', 'id')->whereIn('id', $item->sizes)->get()->toArray();
            }
            return $item;
        });

        $variants = Variants::where('ads_id', $product[0]->id)->get()->map(function ($item, $key) {
            if ($item->colors) {
                $item->colors = Colors::select('title', 'id')->whereIn('id', $item->colors)->get()->toArray();
            }
            if ($item->sizes) {
                $item->sizes = Sizes::select('title', 'id')->whereIn('id', $item->sizes)->get()->toArray();
            }
            return $item;
        });

        $page = Pages::where('slug', 'delivery')->first();

        return view('catalog/view', ['product' => $product[0], 'variants' => $variants, 'page' => $page]);
    }

    /**
     * Catalog sidebar
     *
     * @return \Illuminate\Http\Response
     */
    public static function sidebar($category, $isModal = false)
    {
        if ($category) {
            $categories = $category->getDescendants();
        } else {
            $categories = Categories::roots()->get();
        }

        $colors = Colors::all();

        if ($isModal) {
            return view('catalog/filter-modal', compact('categories', 'colors'));
        }

        return view('catalog/sidebar', compact('categories', 'colors'));
    }

    /**
     * Widget interesting products for you
     * @param  [type] $product [description]
     * @return [type]          [description]
     */
    public static function widgetInteresting($product)
    {
        $products = Ads::interesting($product)->withVariants()->limit(8)->get();
        if ($products->count() < 5) {
            $products = Ads::sortCreated()->withVariants()->limit(8)->get();
        }
        return view('catalog/widget-interesting', compact('products'));
    }

    /**
     * Widget buy products with this
     * @param  [type] $product [description]
     * @return [type]          [description]
     */
    public static function widgetBuyWith($product)
    {
        $products = Ads::buyWith($product)->withVariants()->limit(8)->get();
        if ($products->count() < 5) {
            $products = Ads::sortCreated()->withVariants()->limit(8)->get();
        }
        return view('catalog/widget-buy-with', compact('products'));
    }

    /**
     * Widget new products
     * @return [type]          [description]
     */
    public static function widgetNew()
    {
        $products = Ads::sortCreated()->withVariants()->limit(8)->get();
        return view('catalog/widget-new', compact('products'));
    }

    /**
     * Widget new products
     * @return [type]          [description]
     */
    public static function widgetRecommend()
    {
        $products = Ads::recommended()->withVariants()->sortCreated()->limit(8)->get();
        return view('catalog/widget-recommend', compact('products'));
    }

    public function search(Request $request)
    {
        $productsIds = null;

        if ($request->colors_id || $request->sizes_id) {
            $products = Ads::select('id', 'colors', 'sizes')->get();
            $products = filterByArrayColumnIntersect($products, 'colors', $request->colors_id);
            $products = filterByArrayColumnIntersect($products, 'sizes', $request->sizes_id);

            $productsIds = $products->pluck('id')->toArray();
        }

        $query = Ads::select(
            'ads.id',
            'ads.title',
            'ads.anonce',
            'ads.slug',
            'ads.price',
            'ads.discount_price',
            'ads.images'
        );

        if ((bool)$request->with_discount) {
            $query->whereNotNull('discount_price');
        }
        if (!is_null($productsIds)) {
            $query->whereIn('id', $productsIds);
        }

        if ($request->category_slug) {
            $query->withCategorySlug($request->category_slug);
        }

        if ($request->brands_id) {
            $query->whereIn('brand_id', $request->brands_id);
        }
        if ($request->looks_id) {
            $look = Looks::find($request->looks_id)->first();
            $query->whereIn('id', $look->ads);
        }

        $query->sorting($request->input('sort', Ads::SORT_DEFAULT));

        $count = $query->count();

        $products = $query->groupBy('ads.id')
            ->skip($request->input('start'))
            ->take($request->input('limit'))
            ->get()
            ->map(function ($item, $key) {
                $item->url = $item->url;
                $item->image = route('imagecache', ['product_widget', $item->mainImage]);
                return $item;
            });

        return response()->json([
            'products' => $products,
            'count' => $count
        ]);
    }
}
