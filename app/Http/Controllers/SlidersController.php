<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sliders;

class SlidersController extends Controller
{
    public static function widget()
	{
		$sliders = Sliders::all();
		return view('sliders/widget', compact('sliders'));
	}
}