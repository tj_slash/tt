<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Partners;

class PartnersController extends Controller
{
    public static function widget()
	{
		$partners = Partners::all();
		$faker = \Faker\Factory::create();
		return view('partners/widget', compact('partners', 'faker'));
	}
}
