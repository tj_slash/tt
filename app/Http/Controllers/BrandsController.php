<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Brands;

class BrandsController extends Controller
{
	/**
	 * Brands list
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$brands = Brands::orderBy('order', 'asc')->get();
		return view('brands/index', compact('brands'));
	}

	/**
	 * Brands view
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function view()
	{
		return view('brands/view');
	}

	/**
	 * Brands view
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function get()
	{
		$brands = Brands::orderBy('order', 'asc')->get();
		return response()->json([
			'brands' => $brands
		]);
	}

	/**
	 * Widget brands on main page
	 *
	 * @return \Illuminate\Http\Response
	 */
    public static function widget()
	{
		$brands = Brands::orderBy('order', 'asc')->limit(16)->get();
		return view('brands/widget', compact('brands'));
	}
}
