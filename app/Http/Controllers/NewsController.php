<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Jenssegers\Date\Date;

use App\Models\News;
use Carbon\Carbon;

class NewsController extends Controller
{
	/**
	 * News list
	 *
	 * @return \Illuminate\Http\Response
	 */
    public function index()
	{
		$lastNews = News::main()->sortCreated()->first();

		$news = News::sortCreated()->paginate(6);

		return view('news/index', compact('news', 'lastNews'));
	}

	/**
	 * News view
	 *
	 * @return \Illuminate\Http\Response
	 */
    public function view($slug)
	{
		$news = News::where('slug', $slug)->first();

		return view('news/view', compact('news'));
	}

	/**
	 * Widget for show latest news
	 *
	 * @return \Illuminate\Http\Response
	 */
    public static function widget()
	{
		$news = News::sortCreated()->first();

		return view('news/widget', compact('news'));
	}

	public function get(Request $request)
	{
		$last = false;
		if ($request->input('start') == 0) {

			$last = News::main();

			if ($request->input('month')) {
				$last->whereMonth('created_at', $request->input('month'));
			}

			$last = $last->sortCreated()->limit(1)->get()->map(function($item, $key) {
				$item->url = $item->url;
				$item->big_image = route('imagecache', ['news_big', $item->big_image]);
				$item->date = Date::parse($item->created_at)->format('j F');
				return $item;
			});
		}


		$news = News::sortCreated();

		if ($request->input('month')) {
			$news->whereMonth('created_at', $request->input('month'));
		}

		$news = $news->skip($request->input('start'))
			->take($request->input('limit'))
			->get()
			->map(function($item, $key) {
				$item->url = $item->url;
				$item->image = route('imagecache', ['medium', $item->image]);
				$item->date = Date::parse($item->created_at)->format('j F');
				return $item;
			});

		$count = News::count();

		if ($request->input('start') == 0 && count($last) > 0) {
			return response()->json([
				'last' => $last[0],
				'news' => $news,
				'count' => $count
			]);
		}

		return response()->json([
			'news' => $news,
			'count' => $count
		]);
	}
}
