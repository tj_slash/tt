<?php
namespace App\Http\Controllers;

use App\Models\Brands;
use App\Models\Categories;
use App\Models\News;
use App\Models\Ads;
use App\Models\Pages;
use Vinkla\Instagram\Instagram;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;

use Spatie\Sitemap\Sitemap;

class MainController extends Controller
{
	public function index()
	{
		$faker = \Faker\Factory::create();
		return view('home/index', compact('faker'));
	}

	public function floors()
	{
		$floors = Brands::all()->groupBy('floor');
		return view('home/floors', compact('floors'));
	}

	public static function menu()
	{
		$categories = Categories::all()->toHierarchy();
		return view('include/menu', compact('categories'));
	}

	public static function instagram()
	{
		$instagram = new Instagram(env('INSTAGRAMM_TOKEN'));
		$photos = $instagram->get();
		return view('include/instagram', compact('photos'));
	}

	public function search(Request $request)
	{
		$news = News::search($request->q)->get();
		$ads = Ads::search($request->q)->get();
		return view('search/index', compact('news', 'ads'));
	}

	public function test()
	{
		$sitemap = Sitemap::create()
		    ->add(route('home'))
		    ->add(route('floors'))
		    ->add(route('shares'))
		    ->add(route('contacts'))
		    ->add(route('news'))
		    ->add(route('brands'))
		    ->add(route('catalog'))
		    ->add(route('look-book'));

		News::all()->each(function (News $item) use ($sitemap) {
		    $sitemap->add(route('news.view', [$item->slug]));
		});
		Pages::all()->each(function (Pages $item) use ($sitemap) {
		    $sitemap->add(route('pages.view', [$item->slug]));
		});
		Categories::all()->each(function (Categories $item) use ($sitemap) {
		    $sitemap->add(route('catalog.category.view', [$item->slug]));
		});
		Ads::all()->each(function (Ads $item) use ($sitemap) {
		    $sitemap->add(route('catalog.view', [$item->slug]));
		});

		$sitemap->writeToFile(public_path('sitemap.xml'));

		return response()->make($sitemap->render('xml'))->header('Content-Type', 'text/xml;charset=utf-8');
	}
}