<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Gifts;
use Session;

class GiftsController extends Controller
{
	public function send(Request $request)
	{
        $gift = new Gifts($request->all());
        if (!$gift->save()) {
	        Session::flash('flash_notification.message', 'Произошла ошибка');
	    	Session::flash('flash_notification.level', 'error');
            return redirect()->back();
        }
        Session::flash('flash_notification.message', 'Скоро мы подарим Вам скидку!');
    	Session::flash('flash_notification.level', 'success');
        return redirect()->back();
	}

	/**
	 * Widget banners on main page
	 *
	 * @return \Illuminate\Http\Response
	 */
    public static function widget()
	{
		return view('gifts/widget');
	}
}
