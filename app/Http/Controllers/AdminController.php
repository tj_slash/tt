<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Variants;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function copyVariant($id)
    {
        $variant = Variants::find($id);

        if (!$variant) {
            return redirect('/admin/variants/');
        }

        $newVariant = $variant->toArray();
        $newVariant['title'] .= ' (копия)';

        Variants::create($newVariant);

        return redirect('/admin/ads/' . $variant->ads_id . '/edit');
    }

    /**
     * Method to upload and save images
     * @param Request $request
     * @return string
     */
    public function storeImage(Request $request)
    {
        //Your upload logic
        $path = Storage::disk('uploads')->put(config('sleeping_owl.imagesUploadDirectory'), $request->file('upload'));
        $result = ['url' => asset($path)];

        if ($request->CKEditorFuncNum && $request->CKEditor && $request->langCode) {
            //that handler to upload image CKEditor from Dialog
            $funcNum = $request->CKEditorFuncNum;
            $CKEditor = $request->CKEditor;
            $langCode = $request->langCode;
            $token = $request->ckCsrfToken;

            return view('admin.helper.ckeditor.upload-file', compact('result', 'funcNum', 'CKEditor', 'langCode', 'token'));
        }

        return $result;
    }
}