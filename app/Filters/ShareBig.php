<?php

namespace App\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class ShareBig implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->fit(1170, 240);
    }
}