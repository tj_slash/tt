<?php

Route::get('', ['as' => 'admin.dashboard', function () {
	$content = 'Define your dashboard here.';
	return AdminSection::view($content, 'Dashboard');
}]);

Route::get('information', ['as' => 'admin.information', function () {
	$content = 'Define your information here.';
	return AdminSection::view($content, 'Information');
}]);

Route::any('/variants/{id}/copy', [
    'uses' => '\App\Http\Controllers\AdminController@copyVariant',
]);

Route::post('/images-upload', [
    'as'   => 'upload.image',
    'uses' => "\App\Http\Controllers\AdminController@storeImage"
]);