<?php

namespace App\Admin\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Navigation\Page;
use SleepingOwl\Admin\Form\FormElements;
use SleepingOwl\Admin\Navigation\Badge;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminSection;
use Meta;
use AdminColumnFilter;


/**
 * Class Looks
 *
 * @property \Models/Looks $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Looks extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Look Book';

    /**
     * @var string
     */
    protected $alias = 'look-book';

    /**
     * Initialize section
     */
    public function initialize()
    {
        $this->addToNavigation()
            ->setIcon('fa fa-list');
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::link('title', 'Заголовок'),
                AdminColumn::count('ads', 'Кол-во товаров'),
                AdminColumn::image('mainImage', 'Изображение'),
                AdminColumn::datetime('created_at', 'Добавлено')->setFormat('Y-m-d H:i:s')
            )->paginate(20);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $products = \App\Models\Ads::get()->mapWithKeys(function($item) {
            return [(int)$item['id'] => $item['title'] . ' - ' .$item['anonce']];
        })->all();
        return AdminForm::panel()->addBody([
            AdminFormElement::text('title', 'Заголовок')->required()->addValidationRule('string'),
            AdminFormElement::multiselect('ads', 'Товары', $products)
                ->taggable(),
            AdminFormElement::textarea('text', 'Полное описание')->required(),
            AdminFormElement::images('images', 'Изображения')->setHelpText('Размер изображения 480x600')->required(),
            AdminFormElement::text('slug', 'Алиас'),
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }


}
