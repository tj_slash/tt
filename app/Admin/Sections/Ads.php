<?php

namespace App\Admin\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Display\Extension\FilterInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Navigation\Page;
use SleepingOwl\Admin\Form\FormElements;
use SleepingOwl\Admin\Navigation\Badge;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminSection;
use Meta;
use AdminColumnFilter;


/**
 * Class Ads
 *
 * @property \Models/Ads $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Ads extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Товары';

    /**
     * @var string
     */
    protected $alias = 'ads';

    /**
     * Initialize section
     */
    public function initialize()
    {
        $this->addToNavigation()
            ->setIcon('fa fa-list');
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $datatables = AdminDisplay::datatables();
        $datatables->setHtmlAttribute('class', 'table-primary');
        $datatables->setColumns([
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::link('title', 'Заголовок'),
                AdminColumn::text('price', 'Цена'),
                AdminColumn::text('discount_price', 'Цена со скидкой')->setWidth('100px'),
                AdminColumn::link('category.title', 'Категория'),
                AdminColumn::datetime('created_at', 'Добавлено')->setFormat('Y-m-d H:i:s')
        ])
        ->paginate(20);
        $datatables->setColumnFilters([
                null,
                AdminColumnFilter::text()->setPlaceholder('Заголовок')->setOperator(FilterInterface::CONTAINS),
                null,
                null,
                AdminColumnFilter::select(new \App\Models\Categories, 'Title')->setDisplay('title')->setPlaceholder('Категория')->setColumnName('category_id')
            ])->setPlacement('table.header');
        $datatables->getColumns()->getControlColumn()->setWidth('150px');
        return $datatables;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $sizes = \App\Models\Sizes::get()->mapWithKeys(function($item) {
            return [(int)$item['id'] => $item['title']];
        })->all();

        $colors = \App\Models\Colors::get()->mapWithKeys(function($item) {
            return [(int)$item['id'] => $item['title']];
        })->all();

        $tabs = AdminDisplay::tabbed([
            'Основные настройки' => new \SleepingOwl\Admin\Form\FormElements([
                AdminFormElement::select('category_id', 'Категория', \App\Models\Categories::class)->setDisplay('adminTitle')->required(),
                AdminFormElement::select('brand_id', 'Бренд', \App\Models\Brands::class, 'title')->required(),
                AdminFormElement::text('title', 'Заголовок')->required()->addValidationRule('string'),
                AdminFormElement::text('price', 'Цена')->required(),
                AdminFormElement::text('discount_price', 'Цена со скидкой'),
                AdminFormElement::multiselect('sizes', 'Размеры', $sizes)->taggable(),
                AdminFormElement::multiselect('colors', 'Цвета', $colors)->taggable(),
                AdminFormElement::text('anonce', 'Краткое описание')->required()->addValidationRule('string'),
                AdminFormElement::wysiwyg('description', 'Полное описание')->required(),
                AdminFormElement::images('images', 'Изображения')->setHelpText('Размер изображения 480x600')->required(),
                AdminFormElement::text('slug', 'Алиас'),
            ])
        ]);

        if ($id) {
            $ads = AdminSection::getModel(\App\Models\Variants::class)->fireDisplay(['params' => ['withAds' => $id]]);
            $ads->setParameter('ads_id', $id);
            $tabs->appendTab(new \SleepingOwl\Admin\Form\FormElements([$ads]), 'Комплектации');
        }

        $form = AdminForm::panel();
        $form->addElement($tabs);
        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
        $tabs = AdminDisplay::tabbed([
            'Основные настройки' => new \SleepingOwl\Admin\Form\FormElements([
                AdminFormElement::select('category_id', 'Категория', \App\Models\Categories::class, 'title')->required(),
                AdminFormElement::select('brand_id', 'Бренд', \App\Models\Brands::class, 'title')->required(),
                AdminFormElement::text('title', 'Заголовок')->required()->addValidationRule('string'),
                AdminFormElement::text('price', 'Цена')->required(),
                AdminFormElement::text('discount_price', 'Цена со скидкой'),
                AdminFormElement::select('color_id', 'Цвет', \App\Models\Colors::class, 'title')->nullable(),
                AdminFormElement::select('size_id', 'Размер', \App\Models\Sizes::class, 'title')->nullable(),
                AdminFormElement::text('anonce', 'Краткое описание')->required()->addValidationRule('string'),
                AdminFormElement::wysiwyg('description', 'Полное описание')->required(),
                AdminFormElement::images('images', 'Изображения')->setHelpText('Размер изображения 480x600')->required(),
                AdminFormElement::text('slug', 'Алиас'),
            ])
        ]);

        $form = AdminForm::panel();
        $form->addElement($tabs);
        return $form;
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
