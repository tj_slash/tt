<?php

namespace App\Admin\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Navigation\Page;
use SleepingOwl\Admin\Form\FormElements;
use SleepingOwl\Admin\Navigation\Badge;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminSection;
use Meta;
use AdminColumnFilter;


/**
 * Class AdsColors
 *
 * @property \Models/AdsColors $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class AdsColors extends Section
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Цветовые решения';

    /**
     * @var string
     */
    protected $alias = 'ads/colors';

    /**
     * @return DisplayInterface
     */
    public function onDisplay($params = null)
    {
        $display = AdminDisplay::datatables()
            ->with('color')
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::link('color.title', 'Заголовок'),
                AdminColumn::image('color.image', 'Превью цвета'),
                AdminColumn::custom('Цвет', function(\Illuminate\Database\Eloquent\Model $model) {
                    return '<span class="label" style="background:' . $model->color->hex . '">' . $model->color->hex . '</span>';
                })->setOrderable(function($query, $order){
                    return $query->orderBy('color.hex', $order);
                }),
                AdminColumn::count('images', 'Кол-во изображений'),
                AdminColumn::datetime('created_at', 'Добавлено')->setFormat('Y-m-d H:i:s')
            )->paginate(20);

        if($params){
            if (!empty($params['withAds'])) {
                $display->setScopes(['withAds' , $params['withAds']]);
            }
        }

        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $tabs = AdminDisplay::tabbed([
            'Основные настройки' => new \SleepingOwl\Admin\Form\FormElements([
                AdminFormElement::select('ads_id', 'Товар', \App\Models\Ads::class, 'title')->required(),
                AdminFormElement::select('color_id', 'Цвет', \App\Models\Colors::class, 'title')->required(),
                AdminFormElement::images('images', 'Изображения')->setHelpText('Размер изображения 480x600')->required(),
            ])
        ]);

        if ($id) {
            $ads = AdminSection::getModel(\App\Models\Variants::class)->fireDisplay(['params' => ['withAdsColor' => $id]]);
            $ads->setParameter('ads_color_id', $id);
            $tabs->appendTab(new \SleepingOwl\Admin\Form\FormElements([$ads]), 'Варианты товара');
        }

        $form = AdminForm::panel();
        $form->addElement($tabs);
        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
