<?php

namespace App\Admin\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Navigation\Page;
use SleepingOwl\Admin\Form\FormElements;
use SleepingOwl\Admin\Navigation\Badge;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminSection;
use Meta;
use AdminColumnFilter;

/**
 * Class Shares
 *
 * @property \Models/Shares $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Shares extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Акции';

    /**
     * @var string
     */
    protected $alias = 'shares';

    /**
     * Initialize section
     */
    public function initialize()
    {
        $this->addToNavigation()
            ->setIcon('fa fa-list');
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::image('image', 'Изображение'),
                AdminColumn::link('title', 'Заголовок'),
                AdminColumn::custom('Пол', function(\Illuminate\Database\Eloquent\Model $model) {
                    return $model->sex == \App\Models\Subscribes::MALE ? 'Мужчинам' : 'Женщинам';
                })->setOrderable(function($query, $order){
                    return $query->orderBy('sex', $order);
                }),
                AdminColumn::datetime('created_at', 'Добавлено')->setFormat('Y-m-d H:i:s')
            )->paginate(10);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('title', 'Заголовок')->required()->addValidationRule('string'),
            AdminFormElement::wysiwyg('text', 'Текст')->required()->addValidationRule('string'),
            AdminFormElement::image('image', 'Изображение')->setHelpText('Размер изображения 1170x240')->required(),
            AdminFormElement::select('sex', 'Пол', [
                \App\Models\Shares::MALE => 'Мужчинам',
                \App\Models\Shares::FEMALE => 'Женщинам',
            ])->nullable(),
            AdminFormElement::datetime('stop_date', 'Окончание акции')->setPickerFormat('Y-m-d H:i:s')
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
