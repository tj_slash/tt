<?php

namespace App\Admin\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Navigation\Page;
use SleepingOwl\Admin\Form\FormElements;
use SleepingOwl\Admin\Navigation\Badge;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminSection;
use Meta;
use AdminColumnFilter;

use App\Admin\FormItems\Color;

/**
 * Class Colors
 *
 * @property \Models/Colors $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Colors extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Цвета';

    /**
     * @var string
     */
    protected $alias = 'colors';

    /**
     * Initialize section
     */
    public function initialize()
    {
        $this->addToNavigation()
            ->setIcon('fa fa-list');
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::link('title', 'Заголовок'),
                AdminColumn::custom('Цвет', function(\Illuminate\Database\Eloquent\Model $model) {
                    return '<span class="label" style="background:' . $model->hex . '">' . $model->hex . '</span>';
                })->setOrderable(function($query, $order){
                    return $query->orderBy('hex', $order);
                })
            )->paginate(10);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        Meta::addJs('admin-colors', '//labs.abeautifulsite.net/jquery-minicolors/jquery.minicolors.js', 'admin-default');
        Meta::addCss('admin-colors', '//labs.abeautifulsite.net/jquery-minicolors/jquery.minicolors.css', 'admin-default');

        Meta::addJs('admin-colors-init', asset('js/colors.js'), 'admin-default');
        Meta::addCss('admin-colors-init', asset('css/colors.css'), 'admin-default');

        return AdminForm::panel()->addBody([
            AdminFormElement::text('title', 'Заголовок')->required()->addValidationRule('string'),
            AdminFormElement::text('hex', 'Цвет')->setHtmlAttributes(['data-control' => 'hue', 'class' => 'form-color']),
            AdminFormElement::text('slug', 'Алиас')->setReadOnly(true),
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
