<?php

namespace App\Admin\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Navigation\Page;
use SleepingOwl\Admin\Form\FormElements;
use SleepingOwl\Admin\Navigation\Badge;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminSection;
use Meta;
use AdminColumnFilter;

/**
 * Class Users
 *
 * @property \Models/Users $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Users extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Пользователи';

    /**
     * @var string
     */
    protected $alias = 'users';

    /**
     * Initialize section
     */
    public function initialize()
    {
        $this->addToNavigation()
            ->setIcon('fa fa-list');
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('name', 'Имя'),
                AdminColumn::text('email', 'E-mail'),
                AdminColumn::datetime('created_at', 'Зарегистрирован')->setFormat('Y-m-d H:i:s')
            )->paginate(10);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('name', 'Имя')->required()->addValidationRule('string'),
            AdminFormElement::text('email', 'E-mail')->required()->addValidationRule('string'),
            AdminFormElement::date('birthday', 'День рождения'),
            AdminFormElement::text('phone', 'Номер телефона'),
            AdminFormElement::select('group_id', 'Группа доступа', [
                \App\Models\Users::GROUP_USER => 'Пользователь',
                \App\Models\Users::GROUP_ADMIN => 'Администратор',
            ])->required(),
            AdminFormElement::select('sex', 'Пол', [
                \App\Models\Users::MALE => 'Мужчина',
                \App\Models\Users::FEMALE => 'Женщина',
            ])->nullable(),
            AdminFormElement::checkbox('notify', 'Получать уведомления'),
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
