<?php

namespace App\Admin\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Navigation\Page;
use SleepingOwl\Admin\Form\FormElements;
use SleepingOwl\Admin\Navigation\Badge;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminSection;
use Meta;
use AdminColumnFilter;

/**
 * Class Photoreports
 *
 * @property \Models/Photoreports $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Photoreports extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Фоторепортажи';

    /**
     * @var string
     */
    protected $alias = 'photoreports';

    /**
     * Initialize section
     */
    public function initialize()
    {
        $this->addToNavigation()
            ->setIcon('fa fa-list');
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::image('image', 'Изображение'),
                AdminColumn::image('big_image', 'Большое изображение'),
                AdminColumn::link('title', 'Заголовок'),
                AdminColumn::url('url', 'Ссылка')->setLinkAttributes(['target' => '_blank']),
                AdminColumn::datetime('created_at', 'Добавлено')->setFormat('Y-m-d H:i:s')
            )->paginate(10);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('title', 'Заголовок')->required()->addValidationRule('string'),
            AdminFormElement::image('image', 'Изображение')->setHelpText('Размер изображения 240x180')->required(),
            AdminFormElement::image('big_image', 'Большое изображение')->setHelpText('Размер изображения 780x360')->required(),
            AdminFormElement::wysiwyg('text', 'Текст')->required()->addValidationRule('string'),
            AdminFormElement::images('images', 'Изображения')->required(),
            AdminFormElement::checkbox('is_main', 'Главный фоторепортаж'),
            AdminFormElement::text('slug', 'Алиас')
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
