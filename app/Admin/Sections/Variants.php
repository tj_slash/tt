<?php

namespace App\Admin\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Navigation\Page;
use SleepingOwl\Admin\Form\FormElements;
use SleepingOwl\Admin\Navigation\Badge;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminSection;
use Meta;
use AdminColumnFilter;


/**
 * Class Variants
 *
 * @property \Models/Variants $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Variants extends Section
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Варианты товара';

    /**
     * @var string
     */
    protected $alias = 'ads/variants';

    /**
     * @return DisplayInterface
     */
    public function onDisplay($params = null)
    {
        $display = AdminDisplay::datatables()
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('title', 'Название'),
                AdminColumn::text('price', 'Цена'),
                AdminColumn::text('discount_price', 'Цена со скидкой'),
                AdminColumn::datetime('created_at', 'Добавлено')->setFormat('Y-m-d H:i:s')
            )->paginate(20);

        if($params){
            if (!empty($params['withAds'])) {
                $display->setScopes(['withAds' , $params['withAds']]);
            }
        }

        $button = new \SleepingOwl\Admin\Display\ControlButton(function (\Illuminate\Database\Eloquent\Model $model) {
           return '/admin/variants/' . $model->getKey() . '/copy';
        }, 'Копировать', 50);
        $button->hideText();
        $button->setIcon('fa fa-clone');
        $button->setHtmlAttribute('class', 'btn-success');

        $control = $display->getColumns()->getControlColumn();
        $control->addButton($button)->setWidth('100px');

        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $sizes = \App\Models\Sizes::get()->mapWithKeys(function($item) {
            return [(int)$item['id'] => $item['title']];
        })->all();

        $colors = \App\Models\Colors::get()->mapWithKeys(function($item) {
            return [(int)$item['id'] => $item['title']];
        })->all();

        return AdminForm::panel()->addBody([
            AdminFormElement::hidden('ads_id')->required(),
            AdminFormElement::text('title', 'Заголовок')->required()->addValidationRule('string'),
            AdminFormElement::text('price', 'Цена')->required(),
            AdminFormElement::text('discount_price', 'Цена со скидкой'),
            AdminFormElement::multiselect('sizes', 'Размеры', $sizes)->taggable(),
            AdminFormElement::multiselect('colors', 'Цвета', $colors)->taggable(),
            AdminFormElement::image('image', 'Изображение для комплектации')->setHelpText('Размер изображения 70x70')->required(),
            AdminFormElement::images('images', 'Изображения')->setHelpText('Размер изображения 480x600')->required(),
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
