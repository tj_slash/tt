<?php

namespace App\Admin\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Navigation\Page;
use SleepingOwl\Admin\Form\FormElements;
use SleepingOwl\Admin\Navigation\Badge;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminSection;
use Meta;
use AdminColumnFilter;

/**
 * Class Subscribes
 *
 * @property \Models/Subscribes $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Subscribes extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Подписки';

    /**
     * @var string
     */
    protected $alias = 'subscribes';

    /**
     * Initialize section
     */
    public function initialize()
    {
        $this->addToNavigation()
            ->setIcon('fa fa-list');
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::email('email', 'E-mail'),
                AdminColumn::custom('Пол', function(\Illuminate\Database\Eloquent\Model $model) {
                    return $model->sex == \App\Models\Subscribes::MALE ? 'Мужчинам' : 'Женщинам';
                })->setOrderable(function($query, $order){
                    return $query->orderBy('sex', $order);
                }),
                AdminColumn::datetime('created_at', 'Добавлено')->setFormat('Y-m-d H:i:s')
            )->paginate(10);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
