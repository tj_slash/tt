<?php

namespace App\Providers;

use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;

class AdminSectionsServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $sections = [
        \App\Models\News::class => 'App\Admin\Sections\News',
        \App\Models\Pages::class => 'App\Admin\Sections\Pages',
        \App\Models\Categories::class => 'App\Admin\Sections\Categories',
        \App\Models\Brands::class => 'App\Admin\Sections\Brands',
        \App\Models\Shares::class => 'App\Admin\Sections\Shares',
        \App\Models\Partners::class => 'App\Admin\Sections\Partners',
        \App\Models\Sliders::class => 'App\Admin\Sections\Sliders',
        \App\Models\Ads::class => 'App\Admin\Sections\Ads',
        \App\Models\Banners::class => 'App\Admin\Sections\Banners',
        \App\Models\Subscribes::class => 'App\Admin\Sections\Subscribes',
        \App\Models\Gifts::class => 'App\Admin\Sections\Gifts',
        \App\Models\Feedbacks::class => 'App\Admin\Sections\Feedbacks',
        \App\Models\Colors::class => 'App\Admin\Sections\Colors',
        \App\Models\Looks::class => 'App\Admin\Sections\Looks',
        \App\Models\AdsColors::class => 'App\Admin\Sections\AdsColors',
        \App\Models\Variants::class => 'App\Admin\Sections\Variants',
        \App\Models\Sizes::class => 'App\Admin\Sections\Sizes',
        \App\Models\Photoreports::class => 'App\Admin\Sections\Photoreports',
        \App\Models\Users::class => 'App\Admin\Sections\Users',
    ];

    /**
     * Register sections.
     *
     * @return void
     */
    public function boot(\SleepingOwl\Admin\Admin $admin)
    {
    	//

        parent::boot($admin);
    }
}
