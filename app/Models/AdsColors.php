<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class AdsColors extends Model
{
    use SoftDeletes;
    use ValidatingTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'color_id',
        'ads_id',
        'images',
    ];

    protected $casts = [
        'images' => 'json'
    ];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'color_id' => 'required|integer',
        'ads_id' => 'required|integer'
    ];

    /**
     * Scope ads colors with ads
     * @param \Illuminate\Database\Query
     * @param integer
     * @return \Illuminate\Database\Query
     */
    public function scopeWithAds($query, $ads_id)
    {
        return $query->where('ads_id', $ads_id);
    }

    /**
     * Get color
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function color()
    {
        return $this->belongsTo(\App\Models\Colors::class);
    }
}
