<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;
use Mail;

class Feedbacks extends Model
{
    use SoftDeletes;
    use ValidatingTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'name',
        'text',
    ];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'email' => 'required|string',
        'name' => 'required|string',
        'text' => 'required|string',
    ];

    public static function boot()
    {
        parent::boot();
        self::created(function($model){
            Mail::send('emails.contacts', ['feedback' => $model], function ($m) use ($model) {
                $m->from('info@tt-store.ru', 'TT store');
                $m->to('info@tt-store.ru', 'TT store')->subject('Новое обращение через форму обратной связи');
            });
            return $model;
        });
    }
}