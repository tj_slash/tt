<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;
use Cviebrock\EloquentSluggable\Sluggable;

class Photoreports extends Model
{
    use SoftDeletes;
    use ValidatingTrait;
    use Sluggable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'slug',
        'text',
        'image',
        'big_image',
        'images',
    ];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'title' => 'required|string',
        'text' => 'required|string',
        'image' => 'required',
        'big_image' => 'required',
        'images' => 'required',
    ];

    protected $casts = [
        'images' => 'json'
    ];

    /**
     * Sluggable options
     *
     * @return array
     */
    public function sluggable()
	{
		return ['slug' => ['source' => 'title', 'unique' => true]];
	}

    /**
     * Get url
     * @return string
     */
    public function getUrlAttribute()
    {
        return route('photoreports.view', [$this->slug]);
    }

    /**
     * Get url
     * @return string
     */
    public function getImagessAttribute()
    {
        $images = [];
        if (!empty($this->images)) {
            foreach ($this->images as $key => $value) {
                $images[$key] = '/' . $value;
            }
        }
        return $images;
    }

    /**
     * Sort by created
     * @param \Illuminate\Database\Query
     * @return \Illuminate\Database\Query
     */
    public function scopeSortCreated($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    /**
     * Sort by recommended
     * @param \Illuminate\Database\Query
     * @return \Illuminate\Database\Query
     */
    public function scopeMain($query)
    {
        return $query->where('is_main', true);
    }
}