<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;
use Cviebrock\EloquentSluggable\Sluggable;

class Brands extends Model
{
    use SoftDeletes;
    use ValidatingTrait;
    use Sluggable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'slug',
    ];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'title' => 'required|string',
        'floor' => 'required',
    ];

    /**
     * Sluggable options
     *
     * @return array
     */
    public function sluggable()
	{
		return ['slug' => ['source' => 'title', 'unique' => true]];
	}
}