<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class Gifts extends Model
{
    use SoftDeletes;
    use ValidatingTrait;

    /**
     * @var integer
     */
    const MALE = 1;

    /**
     * @var integer
     */
    const FEMALE = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'sex',
    ];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'email' => 'required|string',
        'sex' => 'required|integer',
    ];
}