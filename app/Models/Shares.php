<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Carbon\Carbon;

class Shares extends Model
{
    use SoftDeletes;
    use ValidatingTrait;
    use Sluggable;

    /**
     * @var integer
     */
    const MALE = 1;

    /**
     * @var integer
     */
    const FEMALE = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'slug',
        'text',
        'image',
        'sex',
    ];

    /**
     * Sluggable options
     *
     * @return array
     */
    public function sluggable()
    {
        return ['slug' => ['source' => 'title', 'unique' => true]];
    }

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'title' => 'required|string',
        'text' => 'required|string',
    ];

    /**
     * Sort by created
     * @param \Illuminate\Database\Query
     * @return \Illuminate\Database\Query
     */
    public function scopeSortCreated($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    /**
     * Get diff milliseconds
     * @return string
     */
    public function getDiffAttribute()
    {
        $start = Carbon::now();
        $stop = Carbon::parse($this->stop_date);
        $diff = $stop->diffInSeconds($start) * 1000;
        return $diff;
    }
}
