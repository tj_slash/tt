<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class Partners extends Model
{
    use SoftDeletes;
    use ValidatingTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'image',
        'url',
    ];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'title' => 'required|string',
        'image' => 'required|string',
        'url' => 'required|string',
    ];

    /**
     * Sort by created
     * @param \Illuminate\Database\Query
     * @return \Illuminate\Database\Query
     */
    public function scopeSortCreated($query)
    {
        return $query->orderBy('created_at', 'desc');
    }
}
