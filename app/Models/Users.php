<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Users extends Authenticatable
{
    use Notifiable;

    const GROUP_USER = 1;

    const GROUP_ADMIN = 2;

    /**
     * @var integer
     */
    const MALE = 1;

    /**
     * @var integer
     */
    const FEMALE = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'group_id',
        'password',
        'sex',
        'phone',
        'birthday',
        'notify'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
