<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Laravel\Scout\Searchable;
use Cviebrock\EloquentSluggable\Services\SlugService;

class Ads extends Model
{
    use SoftDeletes;
    use ValidatingTrait;
    use Sluggable;
    // use Searchable;


    const SORT_DEFAULT = 1;
    const SORT_NEW = 2;
    const SORT_POPULAR = 3;
    const SORT_PRICE_ASC = 4;
    const SORT_PRICE_DESC = 5;

    const SEARCH_INDEX = 'adsIndex';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'category_id',
        'anonce',
        'description',
        'colors',
        'sizes',
        'slug',
    ];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'title' => 'required|string',
        'anonce' => 'required|string',
        'description' => 'required|string',
    ];

    protected $casts = [
        'images' => 'json',
        'colors' => 'json',
        'sizes' => 'json',
    ];

    /**
     * Sluggable options
     *
     * @return array
     */
    public function sluggable()
	{
		return ['slug' => ['source' => 'title', 'unique' => true]];
	}

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return self::SEARCH_INDEX;
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();
        return $array;
    }

    /**
     * Get category
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(\App\Models\Categories::class);
    }

    /**
     * Get category
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo(\App\Models\Brands::class);
    }

    /**
     * Get variants
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function variants()
    {
        return $this->hasMany(\App\Models\Variants::class, 'ads_id');
    }

    /**
     * Get variants
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function adsColors()
    {
        return $this->hasMany(\App\Models\AdsColors::class, 'ads_id');
    }

    /**
     * Get main image
     * @return string
     */
    public function getMainImageAttribute()
    {
        if ($this->images) {
        return current($this->images);
        }
        return '';
    }

    /**
     * Get url
     * @return string
     */
    public function getUrlAttribute()
    {
        return route('catalog.view', [$this->slug]);
    }

    /**
     * Sort by created
     * @param \Illuminate\Database\Query
     * @return \Illuminate\Database\Query
     */
    public function scopeInteresting($query, $product)
    {
        return $query
            ->orderBy('created_at', 'desc')
            ->where('id', '!=', $product->id)
            ->where('category_id', $product->category_id);
    }

    /**
     * Sort by created
     * @param \Illuminate\Database\Query
     * @return \Illuminate\Database\Query
     */
    public function scopeWithVariants($query)
    {
        return $query;
    }

    /**
     * Sort by created
     * @param \Illuminate\Database\Query
     * @return \Illuminate\Database\Query
     */
    public function scopeBuyWith($query, $product)
    {
        return $query
            ->orderBy('created_at', 'desc')
            ->where('id', '!=', $product->id)
            ->where('brand_id', $product->brand_id);
    }

    /**
     * Sort by created
     * @param \Illuminate\Database\Query
     * @return \Illuminate\Database\Query
     */
    public function scopeSortCreated($query)
    {
        return $query->orderBy('ads.created_at', 'desc');
    }

    /**
     * Sort by created
     * @param \Illuminate\Database\Query
     * @return \Illuminate\Database\Query
     */
    public function scopeSorting($query, $sort)
    {
        switch ($sort) {
            case self::SORT_NEW:
                $query->orderBy('ads.created_at', 'desc');
                break;
            case self::SORT_POPULAR:
                $query->orderBy('ads.is_recommend', 'desc');
                break;
            case self::SORT_PRICE_ASC:
                $query->orderBy(\DB::raw('MIN(ads.price)'), 'asc');
                break;
            case self::SORT_PRICE_DESC:
                $query->orderBy(\DB::raw('MIN(ads.price)'), 'desc');
                break;
            default:
                $query->orderBy('ads.is_recommend', 'desc');
                break;
        }
        return $query;
    }

    /**
     * Sort by recommended
     * @param \Illuminate\Database\Query
     * @return \Illuminate\Database\Query
     */
    public function scopeRecommended($query)
    {
        return $query->where('is_recommend', true);
    }

    /**
     * Sort by created
     * @param \Illuminate\Database\Query
     * @return \Illuminate\Database\Query
     */
    public function scopeWithBrandSlug($query, $slug)
    {
        return $query->leftJoin('brands', 'ads.brand_id', '=', 'brands.id')
            ->where('brands.slug', $slug);
    }

    /**
     * With category and descendats
     * @param \Illuminate\Database\Query
     * @return \Illuminate\Database\Query
     */
    public function scopeWithCategorySlug($query, $slug)
    {
        $category = \App\Models\Categories::where('slug', $slug)->first();
        $categories = $category->getDescendantsAndSelf()->pluck('id');
        return $query->whereIn('category_id', $categories);
    }

    /**
     * Sort by recommended
     * @param \Illuminate\Database\Query
     * @return \Illuminate\Database\Query
     */
    public function scopeSearch($query, $q)
    {
        return $query->where('title', 'LIKE', '%' . $q . '%')
            ->orWHere('anonce', 'LIKE', '%' . $q . '%')
            ->orWHere('description', 'LIKE', '%' . $q . '%');
    }


    /**
     * Boot model
     */
    public static function boot()
    {
        self::saving(function($model) {
            if ($model->sizes) {
                $sizes = [];
                foreach ($model->sizes as $key => $value) {
                    $sizes[$key] = (int)$value;
                }
                $model->sizes = $sizes;
            }
            if ($model->colors) {
                $colors = [];
                foreach ($model->colors as $key => $value) {
                    $colors[$key] = (int)$value;
                }
                $model->colors = $colors;
            }
            if (!$model->slug) {
                $model->slug = SlugService::createSlug(Ads::class, 'slug', $model->title);
            }
            return $model;
        });
        parent::boot();
    }
}
