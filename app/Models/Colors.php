<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class Colors extends Model
{
    use SoftDeletes;
    use ValidatingTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'hex'
    ];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'title' => 'required|string',
        'hex' => 'required|string',
    ];
}