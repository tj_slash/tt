<?php

namespace App\Models;

use Baum\Node;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;
use Cviebrock\EloquentSluggable\Sluggable;

class Categories extends Node
{
    use SoftDeletes;
    use ValidatingTrait;
    use Sluggable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'slug',
    ];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'title' => 'required|string',
    ];

    /**
     * Sluggable options
     *
     * @return array
     */
    public function sluggable()
	{
		return ['slug' => ['source' => 'title', 'unique' => true]];
	}


    /**
     * Get adminTitle
     * @return string
     */
    public function getAdminTitleAttribute()
    {
        $title = '';
        foreach($this->getAncestors() as $ancestor) {
            $title.= $ancestor->title . ' - ';
        }
        return $title . $this->title;
    }
}
