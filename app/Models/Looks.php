<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use App\Models\Ads;

class Looks extends Model
{
    use SoftDeletes;
    use ValidatingTrait;
    use Sluggable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'slug',
        'text',
        'ads',
        'images',
    ];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'images' => 'required',
        'ads' => 'required',
        'title' => 'required|string',
        'text' => 'required|string'
    ];

    protected $casts = [
        'ads' => 'json',
        'images' => 'json',
    ];

    /**
     * Sluggable options
     *
     * @return array
     */
    public function sluggable()
	{
		return ['slug' => ['source' => 'title', 'unique' => true]];
	}

    /**
     * Sort by created
     * @param \Illuminate\Database\Query
     * @return \Illuminate\Database\Query
     */
    public function scopeSortCreated($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    /**
     * Get products
     * @return string
     */
    public function getProductsAttribute()
    {
        return Ads::whereIn('id', $this->ads)->get();
    }

    /**
     * Get main image
     * @return string
     */
    public function getMainImageAttribute()
    {
        return current($this->images);
    }

    public static function boot()
    {
        self::saving(function($model) {
            if ($model->ads) {
                $ads = [];
                foreach ($model->ads as $key => $value) {
                    $ads[$key] = (int)$value;
                }
                $model->ads = $ads;
            }
            if (!$model->slug) {
                $model->slug = SlugService::createSlug(Looks::class, 'slug', $model->title);
            }
            return $model;
        });

        parent::boot();
    }
}
