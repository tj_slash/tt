<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class Variants extends Model
{
    use SoftDeletes;
    use ValidatingTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ads_id',
        'title',
        'sizes',
        'colors',
        'images',
        'price',
        'discont_price',
        'image',
    ];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'ads_id' => 'required|integer',
        'price' => 'required|integer',
        'title' => 'required|string',
        'image' => 'required|string',
    ];

    protected $casts = [
        'images' => 'json',
        'sizes' => 'json',
        'colors' => 'json',
    ];

    /**
     * Get ads
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ads()
    {
        return $this->belongsTo(\App\Models\Ads::class);
    }

    /**
     * Scope ads colors with ads
     * @param \Illuminate\Database\Query
     * @param integer
     * @return \Illuminate\Database\Query
     */
    public function scopeWithAds($query, $ads_id)
    {
        return $query->where('ads_id', $ads_id);
    }

    public static function boot()
    {
        self::saving(function($model) {
            if ($model->sizes) {
                $sizes = [];
                foreach ($model->sizes as $key => $value) {
                    $sizes[$key] = (int)$value;
                }
                $model->sizes = $sizes;
            }
            if ($model->colors) {
                $colors = [];
                foreach ($model->colors as $key => $value) {
                    $colors[$key] = (int)$value;
                }
                $model->colors = $colors;
            }
            return $model;
        });
        parent::boot();
    }
}
