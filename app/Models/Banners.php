<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class Banners extends Model
{
    use SoftDeletes;
    use ValidatingTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'image',
        'url',
        'order',
    ];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'title' => 'required|string',
        'image' => 'required|string',
        'url' => 'required|string',
    ];

    /**
     * Sort by created
     * @param \Illuminate\Database\Query
     * @return \Illuminate\Database\Query
     */
    public function scopeSortOrdered($query)
    {
        return $query->orderBy('order', 'asc');
    }
}
