<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Laravel\Scout\Searchable;

class News extends Model
{
    use SoftDeletes;
    use ValidatingTrait;
    use Sluggable;
    // use Searchable;

    const SEARCH_INDEX = 'newsIndex';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'slug',
        'text',
        'image',
        'big_image',
    ];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'title' => 'required|string',
        'text' => 'required|string'
    ];

    /**
     * Sluggable options
     *
     * @return array
     */
    public function sluggable()
	{
		return ['slug' => ['source' => 'title', 'unique' => true]];
	}

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return self::SEARCH_INDEX;
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();
        return $array;
    }

    /**
     * Get url
     * @return string
     */
    public function getUrlAttribute()
    {
        return route('news.view', [$this->slug]);
    }

    /**
     * Sort by created
     * @param \Illuminate\Database\Query
     * @return \Illuminate\Database\Query
     */
    public function scopeSortCreated($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    /**
     * Sort by recommended
     * @param \Illuminate\Database\Query
     * @return \Illuminate\Database\Query
     */
    public function scopeMain($query)
    {
        return $query->where('is_main', true);
    }

    /**
     * Sort by recommended
     * @param \Illuminate\Database\Query
     * @return \Illuminate\Database\Query
     */
    public function scopeSearch($query, $q)
    {
        return $query->where('title', 'LIKE', '%' . $q . '%')
            ->orWHere('text', 'LIKE', '%' . $q . '%');
    }
}