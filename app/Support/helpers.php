<?php
if (!function_exists('renderCatalogNode')) {
    /**
     * @param $node
     * @return string
     */
    function renderCatalogNode($node) {
        $html = '';
        $activeClass = Request::is('catalog/category/' . $node->slug) ? 'active' : '';
        if( $node->isLeaf() ) {
            $html .= '<li><a href="' . route('catalog.category.view', [$node->slug]) . '" title="' . $node->title . '" class="'
            . $activeClass . '"><span>' .  $node->title . '</span></a></li>';
        } else {
            $html .= '<li><a href="' . route('catalog.category.view', [$node->slug]) . '" title="' . $node->title . '" class="'
            . $activeClass . '"><span>' .  $node->title . '</span></a>';

            $html .= '<ul class="menu vertical nested">';

            foreach($node->children as $child)
                $html .= renderCatalogNode($child);

            $html .= '</ul>';

            $html .= '</li>';
        }

        return $html;
    }
}

if (!function_exists('filterByArrayColumnIntersect')) {
    /**
     * return filtered $collection where items value of $columName have intersections with $values
     * @param \Illuminate\Database\Eloquent\Collection $collection
     * @param string $columnName name of column where value stored as array
     * @param array|null $values array of values for intersection
     * @return \Illuminate\Database\Eloquent\Collection
     */
    function filterByArrayColumnIntersect(\Illuminate\Database\Eloquent\Collection $collection, string $columnName, $values)
    {
        if (empty($values)) {
            return $collection;
        }
        return $collection->filter(function ($item, $key) use($values, $columnName) {
            return !empty(array_intersect($item->$columnName, $values));
        });
    }

}