<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDiscountPrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ads', function (Blueprint $table) {
            $table->integer('discount_price')->nullable()->default(null)->after('price');
        });
        Schema::table('variants', function (Blueprint $table) {
            $table->integer('discount_price')->nullable()->default(null)->after('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ads', function (Blueprint $table) {
             $table->dropColumn('discount_price');
        });
        Schema::table('variants', function (Blueprint $table) {
             $table->dropColumn('discount_price');
        });
    }
}
