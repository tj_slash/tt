<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->enum('sex', [
                \App\Models\Users::MALE,
                \App\Models\Users::FEMALE,
            ])->nullable()->default(null);
            $table->string('birthday')->nullable()->default(null);
            $table->string('phone')->nullable()->default(null);
            $table->boolean('notify')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
             $table->dropColumn('sex');
             $table->dropColumn('birthday');
             $table->dropColumn('phone');
             $table->dropColumn('notify');
        });
    }
}
