<?php

use Illuminate\Database\Seeder;
use App\Models\Ads;
use App\Models\Colors;
use App\Models\Variants;
use App\Models\Sizes;

class AdsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Ads::truncate();

        DB::table('variants')->truncate();

        $faker = \Faker\Factory::create();

        factory(Ads::class, 10)->create()->each(function($ads) {
            $adsColor = factory(Variants::class, 3)->create(['ads_id' => $ads->id]);
        });
    }
}