<?php

use Illuminate\Database\Seeder;
use App\Models\Looks;

class LooksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Looks::truncate();

        factory(Looks::class, 6)->create();
    }
}
