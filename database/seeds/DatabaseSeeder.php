<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(NewsSeeder::class);
        $this->call(PagesSeeder::class);
        $this->call(CategoriesSeeder::class);
        $this->call(BrandsSeeder::class);
        $this->call(SharesSeeder::class);
        $this->call(PartnersSeeder::class);
        $this->call(SlidersSeeder::class);
        $this->call(BannersSeeder::class);
        $this->call(SubscribesSeeder::class);
        $this->call(GiftsSeeder::class);
        $this->call(FeedbacksSeeder::class);
        $this->call(ColorsSeeder::class);
        $this->call(SizesSeeder::class);
        $this->call(AdsSeeder::class);
        $this->call(LooksSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(PhotoreportsSeeder::class);
    }
}