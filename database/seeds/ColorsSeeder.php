<?php

use Illuminate\Database\Seeder;
use App\Models\Colors;

class ColorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Colors::truncate();
        Colors::insert(['title' => 'Синий', 'hex' => '#0000FF', 'image' => '/images/colors/1.png']);
        Colors::insert(['title' => 'Красный', 'hex' => '#FF0000', 'image' => '/images/colors/2.png']);
        Colors::insert(['title' => 'Серый', 'hex' => '#808080', 'image' => '/images/colors/3.png']);
        Colors::insert(['title' => 'Белый', 'hex' => '#FFFFFF', 'image' => '/images/colors/4.png']);
        Colors::insert(['title' => 'Коричневый', 'hex' => '#a52a2a', 'image' => '/images/colors/5.png']);
        Colors::insert(['title' => 'Черный', 'hex' => '#000000', 'image' => '/images/colors/6.png']);
        Colors::insert(['title' => 'Бежевый', 'hex' => '#f5f5dc', 'image' => '/images/colors/7.png']);
        Colors::insert(['title' => 'Зеленый', 'hex' => '#008000', 'image' => '/images/colors/8.png']);
        Colors::insert(['title' => 'Фиолетовый', 'hex' => '#ee82ee', 'image' => '/images/colors/9.png']);
        Colors::insert(['title' => 'Желтый', 'hex' => '#ffff00', 'image' => '/images/colors/10.png']);
        Colors::insert(['title' => 'Розовый', 'hex' => '#ffc0cb', 'image' => '/images/colors/12.png']);
        Colors::insert(['title' => 'Хаки', 'hex' => '#f0e68c', 'image' => '/images/colors/13.png']);
        Colors::insert(['title' => 'Разноцветный', 'hex' => '#cd7f32', 'image' => '/images/colors/14.png']);
        Colors::insert(['title' => 'Оранжевый', 'hex' => '#cd7f32', 'image' => '/images/colors/15.png']);
        Colors::insert(['title' => 'Черно-белый', 'hex' => '#000000', 'image' => '/images/colors/16.png']);
        Colors::insert(['title' => 'Леопардовый', 'hex' => '#cd7f32', 'image' => '/images/colors/18.png']);
    }
}
