<?php

use Illuminate\Database\Seeder;
use App\Models\Pages;

class PagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pages::truncate();
        factory(Pages::class, 20)->create();
        factory(Pages::class)->create(['slug' => 'about', 'title' => 'О магазине']);
        factory(Pages::class)->create(['slug' => 'photoreport', 'title' => 'Фоторепортажи']);
        factory(Pages::class)->create(['slug' => 'history', 'title' => 'История']);
        factory(Pages::class)->create(['slug' => 'bonus', 'title' => 'Бонусная программа']);
        factory(Pages::class)->create(['slug' => 'collaboration', 'title' => 'Сотрудничество']);
        factory(Pages::class)->create(['slug' => 'privacy-policy', 'title' => 'Политика конфиденциальности']);
        factory(Pages::class)->create(['slug' => 'info', 'title' => 'Информация для покупателей']);
    }
}
