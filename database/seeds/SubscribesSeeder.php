<?php

use Illuminate\Database\Seeder;
use App\Models\Subscribes;

class SubscribesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subscribes::truncate();
        factory(Subscribes::class, 20)->create();
    }
}
