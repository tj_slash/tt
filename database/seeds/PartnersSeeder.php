<?php

use Illuminate\Database\Seeder;
use App\Models\Partners;

class PartnersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Partners::truncate();
        factory(Partners::class, 20)->create();
    }
}
