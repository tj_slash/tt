<?php

use Illuminate\Database\Seeder;
use App\Models\Brands;

class BrandsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Brands::truncate();
        factory(Brands::class)->create(['title' => 'Aerowatch', 'floor' => 1]);
        factory(Brands::class)->create(['title' => 'AnnaMaria Cammilli', 'floor' => 1]);
        factory(Brands::class)->create(['title' => 'Balmain', 'floor' => 1]);
        factory(Brands::class)->create(['title' => 'Brumani', 'floor' => 1]);
        factory(Brands::class)->create(['title' => 'Calvin Klein', 'floor' => 1]);
        factory(Brands::class)->create(['title' => 'Cartier', 'floor' => 1]);
        factory(Brands::class)->create(['title' => 'Edox', 'floor' => 1]);
        factory(Brands::class)->create(['title' => 'Graziella', 'floor' => 1]);
        factory(Brands::class)->create(['title' => 'Hamilton', 'floor' => 1]);
        factory(Brands::class)->create(['title' => 'KotaOsta', 'floor' => 1]);
        factory(Brands::class)->create(['title' => 'Longines', 'floor' => 1]);
        factory(Brands::class)->create(['title' => 'Mido', 'floor' => 1]);
        factory(Brands::class)->create(['title' => 'Montblanc', 'floor' => 1]);
        factory(Brands::class)->create(['title' => 'Omega', 'floor' => 1]);
        factory(Brands::class)->create(['title' => 'Rado', 'floor' => 1]);
        factory(Brands::class)->create(['title' => 'Swarovski', 'floor' => 1]);
        factory(Brands::class)->create(['title' => 'Swatch', 'floor' => 1]);
        factory(Brands::class)->create(['title' => 'TAG Heuer', 'floor' => 1]);
        factory(Brands::class)->create(['title' => 'Thomas Sabo', 'floor' => 1]);
        factory(Brands::class)->create(['title' => 'Tissot', 'floor' => 1]);
        factory(Brands::class)->create(['title' => 'Ulysse Nardin', 'floor' => 1]);
        factory(Brands::class)->create(['title' => 'Арт Премьер', 'floor' => 1]);
        factory(Brands::class)->create(['title' => 'Бриллианты Костромы', 'floor' => 1]);
        factory(Brands::class)->create(['title' => 'МЭЮЗ', 'floor' => 1]);

        factory(Brands::class)->create(['title' => 'Stefanel', 'floor' => 2]);
        factory(Brands::class)->create(['title' => 'Marc O\'Polo', 'floor' => 2]);
        factory(Brands::class)->create(['title' => 'New Balance', 'floor' => 2]);
        factory(Brands::class)->create(['title' => 'Timberland', 'floor' => 2]);
        factory(Brands::class)->create(['title' => 'Juicy Couture', 'floor' => 2]);

        factory(Brands::class)->create(['title' => 'AngySix', 'floor' => 3]);
        factory(Brands::class)->create(['title' => 'Del Mare', 'floor' => 3]);
        factory(Brands::class)->create(['title' => 'INVIDIA', 'floor' => 3]);
        factory(Brands::class)->create(['title' => 'Ungaro', 'floor' => 3]);
        factory(Brands::class)->create(['title' => 'MARIELLA ARDUINI', 'floor' => 3]);
        factory(Brands::class)->create(['title' => 'ADYVA', 'floor' => 3]);
        factory(Brands::class)->create(['title' => 'Rivela', 'floor' => 3]);
        factory(Brands::class)->create(['title' => 'Desco', 'floor' => 3]);
        factory(Brands::class)->create(['title' => 'Carrera', 'floor' => 3]);
        factory(Brands::class)->create(['title' => 'SIMAR', 'floor' => 3]);
        factory(Brands::class)->create(['title' => 'Essenza', 'floor' => 3]);
        factory(Brands::class)->create(['title' => 'Tosca Blu', 'floor' => 3]);
        factory(Brands::class)->create(['title' => 'Cerruti', 'floor' => 3]);
    }
}
