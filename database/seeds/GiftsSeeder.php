<?php

use Illuminate\Database\Seeder;
use App\Models\Gifts;

class GiftsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Gifts::truncate();
        factory(Gifts::class, 20)->create();
    }
}
