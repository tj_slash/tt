<?php

use Illuminate\Database\Seeder;
use App\Models\Sliders;

class SlidersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sliders::truncate();
        factory(Sliders::class, 5)->create();
    }
}
