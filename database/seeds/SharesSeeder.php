<?php

use Illuminate\Database\Seeder;
use App\Models\Shares;

class SharesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Shares::truncate();
        factory(Shares::class, 20)->create();
    }
}
