<?php

use Illuminate\Database\Seeder;
use App\Models\Banners;

class BannersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Banners::truncate();

        $faker = Faker\Factory::create();

        Banners::create([
        	'title' => 'Верхний большой баннер 570x270',
        	'image' => '/images/uploads/' . $faker->image('public/images/uploads/', 570, 270, 'fashion', false),
        	'order' => 1,
        	'url' => $faker->url()
    	]);

        Banners::create([
        	'title' => 'Верхний маленький баннер 270x270',
        	'image' => '/images/uploads/' . $faker->image('public/images/uploads/', 270, 270, 'fashion', false),
        	'order' => 2,
        	'url' => $faker->url()
    	]);
        Banners::create([
            'title' => 'Верхний маленький баннер 270x270',
        	'image' => '/images/uploads/' . $faker->image('public/images/uploads/', 270, 270, 'fashion', false),
        	'order' => 3,
        	'url' => $faker->url()
    	]);

        Banners::create([
            'title' => 'Нижний маленький баннер 270x180',
        	'image' => '/images/uploads/' . $faker->image('public/images/uploads/', 270, 180, 'fashion', false),
        	'order' => 4,
        	'url' => $faker->url()
    	]);
        Banners::create([
            'title' => 'Нижний маленький баннер 270x180',
        	'image' => '/images/uploads/' . $faker->image('public/images/uploads/', 270, 180, 'fashion', false),
        	'order' => 5,
        	'url' => $faker->url()
    	]);
    }
}
