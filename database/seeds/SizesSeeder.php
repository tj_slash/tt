<?php

use Illuminate\Database\Seeder;
use App\Models\Sizes;

class SizesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sizes::truncate();
        for ($size = 44; $size <= 62; $size += 2) {
            Sizes::insert(['title' => $size]);
        }
    }
}
