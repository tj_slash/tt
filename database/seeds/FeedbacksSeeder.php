<?php

use Illuminate\Database\Seeder;
use App\Models\Feedbacks;

class FeedbacksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Feedbacks::truncate();
        factory(Feedbacks::class, 20)->create();
    }
}
