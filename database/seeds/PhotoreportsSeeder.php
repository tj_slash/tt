<?php

use Illuminate\Database\Seeder;
use App\Models\Photoreports;

class PhotoreportsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Photoreports::truncate();
        factory(Photoreports::class, 20)->create();
    }
}
