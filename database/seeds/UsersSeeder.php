<?php

use Illuminate\Database\Seeder;
use App\Models\Users;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Users::truncate();

        Users::create([
            'name' => 'admin',
            'group_id' => Users::GROUP_ADMIN,
            'email' => 'admin@site.com',
            'password' => bcrypt('password')
        ]);
    }
}
