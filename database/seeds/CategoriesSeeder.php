<?php

use Illuminate\Database\Seeder;
use App\Models\Categories;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Categories::truncate();

        $female = factory(Categories::class)->create(['title' => 'Женское']);

        $clothes = factory(Categories::class)->create(['title' => 'Одежда', 'parent_id' => $female->id]);
        factory(Categories::class)->create(['title' => 'Платья', 'parent_id' => $clothes->id]);
    	factory(Categories::class)->create(['title' => 'Футболки и топы', 'parent_id' => $clothes->id]);
    	factory(Categories::class)->create(['title' => 'Брюки и шорты', 'parent_id' => $clothes->id]);
    	factory(Categories::class)->create(['title' => 'Блузки и рубашки', 'parent_id' => $clothes->id]);
    	factory(Categories::class)->create(['title' => 'Джемперы и кардиганы', 'parent_id' => $clothes->id]);
    	factory(Categories::class)->create(['title' => 'Лонгсливы', 'parent_id' => $clothes->id]);
    	factory(Categories::class)->create(['title' => 'Юбки', 'parent_id' => $clothes->id]);
    	factory(Categories::class)->create(['title' => 'Верхняя одежда', 'parent_id' => $clothes->id]);
    	factory(Categories::class)->create(['title' => 'Джинсы', 'parent_id' => $clothes->id]);
    	factory(Categories::class)->create(['title' => 'Туники', 'parent_id' => $clothes->id]);
    	factory(Categories::class)->create(['title' => 'Комбинезоны', 'parent_id' => $clothes->id]);
    	factory(Categories::class)->create(['title' => 'Костюмы', 'parent_id' => $clothes->id]);
    	factory(Categories::class)->create(['title' => 'Жилеты', 'parent_id' => $clothes->id]);
    	factory(Categories::class)->create(['title' => 'Толстовки', 'parent_id' => $clothes->id]);
    	factory(Categories::class)->create(['title' => 'Худи', 'parent_id' => $clothes->id]);
    	factory(Categories::class)->create(['title' => 'Водолазки', 'parent_id' => $clothes->id]);
    	factory(Categories::class)->create(['title' => 'Свитшоты', 'parent_id' => $clothes->id]);

        $footwear = factory(Categories::class)->create(['title' => 'Обувь', 'parent_id' => $female->id]);
        factory(Categories::class)->create(['title' => 'Кеды и кроссовки', 'parent_id' => $footwear->id]);
        factory(Categories::class)->create(['title' => 'Шлепанцы', 'parent_id' => $footwear->id]);
        factory(Categories::class)->create(['title' => 'Мокасины', 'parent_id' => $footwear->id]);
        factory(Categories::class)->create(['title' => 'Ботинки и ботильоны', 'parent_id' => $footwear->id]);
        factory(Categories::class)->create(['title' => 'Сандалии', 'parent_id' => $footwear->id]);
        factory(Categories::class)->create(['title' => 'Слипоны', 'parent_id' => $footwear->id]);

        $male = factory(Categories::class)->create(['title' => 'Мужское']);

        $clothes = factory(Categories::class)->create(['title' => 'Одежда', 'parent_id' => $male->id]);
    	factory(Categories::class)->create(['title' => 'Брюки и шорты', 'parent_id' => $clothes->id]);
    	factory(Categories::class)->create(['title' => 'Рубашки', 'parent_id' => $clothes->id]);
    	factory(Categories::class)->create(['title' => 'Джинсы', 'parent_id' => $clothes->id]);
    	factory(Categories::class)->create(['title' => 'Верхняя одежда', 'parent_id' => $clothes->id]);
    	factory(Categories::class)->create(['title' => 'Джемперы и кардиганы', 'parent_id' => $clothes->id]);
    	factory(Categories::class)->create(['title' => 'Лонгсливы', 'parent_id' => $clothes->id]);
    	factory(Categories::class)->create(['title' => 'Толстовки', 'parent_id' => $clothes->id]);
    	factory(Categories::class)->create(['title' => 'Худи', 'parent_id' => $clothes->id]);
    	factory(Categories::class)->create(['title' => 'Костюмы', 'parent_id' => $clothes->id]);
    	factory(Categories::class)->create(['title' => 'Пиджаки и жакеты', 'parent_id' => $clothes->id]);
    	factory(Categories::class)->create(['title' => 'Жилеты', 'parent_id' => $clothes->id]);
    	factory(Categories::class)->create(['title' => 'Свитшоты', 'parent_id' => $clothes->id]);
    	factory(Categories::class)->create(['title' => 'Водолазки', 'parent_id' => $clothes->id]);

        $footwear = factory(Categories::class)->create(['title' => 'Одежда', 'parent_id' => $male->id]);
        factory(Categories::class)->create(['title' => 'Ботинки', 'parent_id' => $footwear->id]);
        factory(Categories::class)->create(['title' => 'Кеды', 'parent_id' => $footwear->id]);
        factory(Categories::class)->create(['title' => 'Кроссовки', 'parent_id' => $footwear->id]);
        factory(Categories::class)->create(['title' => 'Туфли', 'parent_id' => $footwear->id]);
        factory(Categories::class)->create(['title' => 'Мокасины', 'parent_id' => $footwear->id]);
        factory(Categories::class)->create(['title' => 'Шлепанцы', 'parent_id' => $footwear->id]);
        factory(Categories::class)->create(['title' => 'Слипоны', 'parent_id' => $footwear->id]);

        $accessories = factory(Categories::class)->create(['title' => 'Аксессуары']);

        $female = factory(Categories::class)->create(['title' => 'Женщинам', 'parent_id' => $accessories->id]);
        factory(Categories::class)->create(['title' => 'Бижутерия', 'parent_id' => $female->id]);
        factory(Categories::class)->create(['title' => 'Головные уборы', 'parent_id' => $female->id]);
        factory(Categories::class)->create(['title' => 'Сумки и рюкзаки', 'parent_id' => $female->id]);
        factory(Categories::class)->create(['title' => 'Ремни и пояса', 'parent_id' => $female->id]);
        factory(Categories::class)->create(['title' => 'Платки и шарфы', 'parent_id' => $female->id]);
        factory(Categories::class)->create(['title' => 'Очки', 'parent_id' => $female->id]);
        factory(Categories::class)->create(['title' => 'Кошельки и кредитницы', 'parent_id' => $female->id]);
        factory(Categories::class)->create(['title' => 'Обложки и ключницы', 'parent_id' => $female->id]);
        factory(Categories::class)->create(['title' => 'Перчатки и варежки', 'parent_id' => $female->id]);
        factory(Categories::class)->create(['title' => 'Пишущие принадлежности', 'parent_id' => $female->id]);

        $male = factory(Categories::class)->create(['title' => 'Мужчинам', 'parent_id' => $accessories->id]);
        factory(Categories::class)->create(['title' => 'Украшения', 'parent_id' => $male->id]);
        factory(Categories::class)->create(['title' => 'Галстуки и бабочки', 'parent_id' => $male->id]);
        factory(Categories::class)->create(['title' => 'Головные уборы', 'parent_id' => $male->id]);
        factory(Categories::class)->create(['title' => 'Сумки и рюкзаки', 'parent_id' => $male->id]);
        factory(Categories::class)->create(['title' => 'Очки', 'parent_id' => $male->id]);
        factory(Categories::class)->create(['title' => 'Кошельки и кредитницы', 'parent_id' => $male->id]);
        factory(Categories::class)->create(['title' => 'Обложки и ключницы', 'parent_id' => $male->id]);
        factory(Categories::class)->create(['title' => 'Перчатки и варежки', 'parent_id' => $male->id]);
        factory(Categories::class)->create(['title' => 'Ремни и пояса', 'parent_id' => $male->id]);
        factory(Categories::class)->create(['title' => 'Шарфы и снуды', 'parent_id' => $male->id]);
        factory(Categories::class)->create(['title' => 'Пишущие принадлежности', 'parent_id' => $male->id]);

        $clock = factory(Categories::class)->create(['title' => 'Часы']);
    	factory(Categories::class)->create(['title' => 'Мужские', 'parent_id' => $clock->id]);
    	factory(Categories::class)->create(['title' => 'Женские', 'parent_id' => $clock->id]);
    	factory(Categories::class)->create(['title' => 'Детские', 'parent_id' => $clock->id]);
    	factory(Categories::class)->create(['title' => 'Смарт-часы', 'parent_id' => $clock->id]);

        $decoration = factory(Categories::class)->create(['title' => 'Украшения']);
    	factory(Categories::class)->create(['title' => 'Кольца', 'parent_id' => $decoration->id]);
    	factory(Categories::class)->create(['title' => 'Серьги', 'parent_id' => $decoration->id]);
    	factory(Categories::class)->create(['title' => 'Подвески', 'parent_id' => $decoration->id]);
    	factory(Categories::class)->create(['title' => 'Браслеты', 'parent_id' => $decoration->id]);
    	factory(Categories::class)->create(['title' => 'Цепи', 'parent_id' => $decoration->id]);
    	factory(Categories::class)->create(['title' => 'Колье', 'parent_id' => $decoration->id]);
    	factory(Categories::class)->create(['title' => 'Броши', 'parent_id' => $decoration->id]);
    	factory(Categories::class)->create(['title' => 'Зажимы для галстука', 'parent_id' => $decoration->id]);
    	factory(Categories::class)->create(['title' => 'Запонки', 'parent_id' => $decoration->id]);
    }
}
