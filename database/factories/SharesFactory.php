<?php

use App\Models\Shares;

$factory->define(Shares::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ru_RU');
    return [
        'title' => $faker->sentence(),
        'text' => $faker->paragraph(),
        'image' => '/images/uploads/' . $faker->image('public/images/uploads/', 1180, 240, 'fashion', false),
        'sex' => $faker->randomElement([
            Shares::MALE,
            Shares::FEMALE,
        ]),
        'stop_date' => $faker->dateTimeBetween('now', '+30 days')
    ];
});
