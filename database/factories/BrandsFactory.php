<?php

use App\Models\Brands;

$factory->define(Brands::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ru_RU');
    return [
        'title' => $faker->sentence(),
        'image' => '/images/uploads/' . $faker->image('public/images/uploads/', 170, 90, 'fashion', false),
    ];
});