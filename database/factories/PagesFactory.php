<?php

use App\Models\Pages;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(Pages::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ru_RU');
    return [
        'title' => $faker->sentence(),
        'text' => $faker->paragraph(),
    ];
});
