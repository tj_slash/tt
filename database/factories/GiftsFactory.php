<?php

use App\Models\Gifts;

$factory->define(Gifts::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ru_RU');
    return [
        'email' => $faker->unique()->safeEmail,
        'sex' => $faker->randomElement([
            Gifts::MALE,
            Gifts::FEMALE,
        ])
    ];
});
