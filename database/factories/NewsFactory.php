<?php

use App\Models\News;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(News::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ru_RU');
    return [
        'title' => $faker->sentence(),
        'text' => $faker->paragraph(),
        'image' => '/images/uploads/' . $faker->image('public/images/uploads/', 270, 180, 'fashion', false),
        'big_image' => '/images/uploads/' . $faker->image('public/images/uploads/', 780, 360, 'fashion', false),
        'is_main' => $faker->boolean(10),
    ];
});
