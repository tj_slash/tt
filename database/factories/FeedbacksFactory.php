<?php

use App\Models\Feedbacks;

$factory->define(Feedbacks::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ru_RU');
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'text' => $faker->paragraph(),
    ];
});
