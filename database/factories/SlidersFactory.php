<?php

use App\Models\Sliders;

$factory->define(Sliders::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ru_RU');
    return [
        'title' => $faker->sentence(),
        'image' => '/images/uploads/' . $faker->image('public/images/uploads/', 1170, 400, 'fashion', false),
        'url' => $faker->url(),
    ];
});
