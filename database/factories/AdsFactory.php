<?php

use App\Models\Ads;
use App\Models\Brands;
use App\Models\Categories;
use App\Models\Colors;
use App\Models\Sizes;

$factory->define(Ads::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ru_RU');

    $categories = Categories::all();
    $brands = Brands::all();
    $colors = Colors::all();
    $sizes = Sizes::all();

    return [
        'title' => $faker->sentence(),
        'slug' => $faker->slug(),
        'anonce' => $faker->sentence(),
        'description' => $faker->paragraph(),
        'category_id' => $categories[rand(0, count($categories) - 1)]->id,
        'brand_id' => $brands[rand(0, count($brands) - 1)]->id,
        'is_recommend' => $faker->boolean(30),
        'price' => $faker->randomNumber(4),
        'images' => [
            'images/uploads/' . $faker->image('public/images/uploads/', 480, 600, 'fashion', false),
            'images/uploads/' . $faker->image('public/images/uploads/', 480, 600, 'fashion', false),
            'images/uploads/' . $faker->image('public/images/uploads/', 480, 600, 'fashion', false),
        ],
        'colors' => [
            $colors[rand(0, count($colors) - 1)]->id,
            $colors[rand(0, count($colors) - 1)]->id,
            $colors[rand(0, count($colors) - 1)]->id
        ],
        'sizes' => [
            $sizes[rand(0, count($sizes) - 1)]->id,
            $sizes[rand(0, count($sizes) - 1)]->id,
            $sizes[rand(0, count($sizes) - 1)]->id
        ]
    ];
});