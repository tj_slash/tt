<?php

use App\Models\Categories;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(Categories::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ru_RU');
    return [
        'title' => $faker->sentence(),
    ];
});
