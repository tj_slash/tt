<?php

use App\Models\AdsColors;
use App\Models\Ads;
use App\Models\Colors;

$factory->define(AdsColors::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ru_RU');

    $ads = Ads::all();
    $colors = Colors::all();

    return [
        'ads_id' => count($ads) > 0 ? $ads[rand(0, count($ads) - 1)]->id : 1,
        'color_id' => $colors[rand(0, count($colors) - 1)]->id,
     //    'images' => $images = [
	    // 	'/images/uploads/' . $faker->image('public/images/uploads/', 480, 600, 'fashion', false),
	    // 	'/images/uploads/' . $faker->image('public/images/uploads/', 480, 600, 'fashion', false),
	    // 	'/images/uploads/' . $faker->image('public/images/uploads/', 480, 600, 'fashion', false),
	    // ]
    ];
});