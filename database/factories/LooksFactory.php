<?php

use App\Models\Ads;
use App\Models\Looks;

$factory->define(Looks::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ru_RU');

    $ads = Ads::all();

    return [
        'title' => $faker->sentence(),
        'text' => $faker->paragraph(),
        'ads' => [
        	$ads[rand(0, count($ads) - 1)]->id,
        	$ads[rand(0, count($ads) - 1)]->id,
        	$ads[rand(0, count($ads) - 1)]->id,
        	$ads[rand(0, count($ads) - 1)]->id,
        	$ads[rand(0, count($ads) - 1)]->id,
        ],
        'images' => [
            '/images/uploads/' . $faker->image('public/images/uploads/', 480, 600, 'fashion', false),
            '/images/uploads/' . $faker->image('public/images/uploads/', 480, 600, 'fashion', false),
            '/images/uploads/' . $faker->image('public/images/uploads/', 480, 600, 'fashion', false),
        ],
    ];
});