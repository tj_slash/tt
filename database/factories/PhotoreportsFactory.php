<?php

use App\Models\Photoreports;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(Photoreports::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ru_RU');
dd($faker->image('public/images/uploads/', 270, 180, 'fashion', false));
    return [
        'title' => $faker->sentence(),
        'text' => $faker->paragraph(),
        'image' => '/images/uploads/' . $faker->image('public/images/uploads/', 270, 180, 'fashion', false),
        'big_image' => '/images/uploads/' . $faker->image('public/images/uploads/', 780, 360, 'fashion', false),
        'is_main' => $faker->boolean(10),
       	'images' => $images = [
	    	'/images/uploads/' . $faker->image('public/images/uploads/', 480, 600, 'fashion', false),
	    	'/images/uploads/' . $faker->image('public/images/uploads/', 480, 600, 'fashion', false),
	    	'/images/uploads/' . $faker->image('public/images/uploads/', 480, 600, 'fashion', false),
	    ]
    ];
});
