<?php

use App\Models\Colors;
use App\Models\Sizes;
use App\Models\Variants;

$factory->define(Variants::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ru_RU');

    $colors = Colors::all();
    $sizes = Sizes::all();

    return [
    	'title' => $faker->sentence(),
        'colors' => [
            $colors[rand(0, count($colors) - 1)]->id,
            $colors[rand(0, count($colors) - 1)]->id,
            $colors[rand(0, count($colors) - 1)]->id
        ],
        'sizes' => [
            $sizes[rand(0, count($sizes) - 1)]->id,
            $sizes[rand(0, count($sizes) - 1)]->id,
            $sizes[rand(0, count($sizes) - 1)]->id
        ],
        'price' => $faker->randomNumber(4),
        'images' => [
            'images/uploads/' . $faker->image('public/images/uploads/', 480, 600, 'fashion', false),
            'images/uploads/' . $faker->image('public/images/uploads/', 480, 600, 'fashion', false),
            'images/uploads/' . $faker->image('public/images/uploads/', 480, 600, 'fashion', false),
        ],
        'image' => 'images/uploads/' . $faker->image('public/images/uploads/', 100, 100, 'fashion', false),
    ];
});