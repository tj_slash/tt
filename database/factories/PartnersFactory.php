<?php

use App\Models\Partners;

$factory->define(Partners::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ru_RU');
    return [
        'title' => $faker->sentence(),
        'image' => '/images/uploads/' . $faker->image('public/images/uploads/', 370, 190, 'fashion', false),
        'url' => $faker->url(),
    ];
});
