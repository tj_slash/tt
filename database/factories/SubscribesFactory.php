<?php

use App\Models\Subscribes;

$factory->define(Subscribes::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ru_RU');
    return [
        'email' => $faker->unique()->safeEmail,
        'sex' => $faker->randomElement([
            Subscribes::MALE,
            Subscribes::FEMALE,
        ])
    ];
});
