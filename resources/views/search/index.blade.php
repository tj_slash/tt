@extends('layouts/general')

@section('content')
    <div class="grid-container margin-top-2 hide-for-small-only">
        <div class="grid-x grid-margin-x">
            <div class="cell">
                <nav role="navigation">
                    {!! Breadcrumbs::render('search') !!}
                </nav>
            </div>
        </div>
    </div>
    <article class="margin-bottom-3">
        @if ($ads->isEmpty() && $news->isEmpty())
            <div class="title-line without-line">
                <span class="font-italic">Ничего не найдено</span>
            </div>
        @endif
        @if (!$ads->isEmpty())
            <div class="title-line without-line">
                <span class="font-italic">Товары</span>
            </div>
            <div class="grid-container margin-top-2">
                <div class="grid-x grid-margin-x">
                    @foreach ($ads as $item)
                        <div class="cell small-12 medium-3 margin-bottom-1 text-center medium-text-left">
                            <a href="{{ route('catalog.view', [$item->slug]) }}" title="{{ $item->title }}">
                                {!! Html::image(route('imagecache', ['medium', $item->mainImage]), null, ['alt' => $item->title, 'title' => $item->title]) !!}
                            </a>
                        </div>
                        <div class="cell small-12 medium-9 margin-bottom-1">
                            <a href="{{ route('catalog.view', [$item->slug]) }}" title="{{ $item->title }}">
                                {{ $item->title }}
                            </a>
                            <p>{{ str_limit($item->description, 400) }}</p>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
        @if (!$news->isEmpty())
            <div class="title-line without-line">
                <span class="font-italic">Новости</span>
            </div>
            <div class="grid-container margin-top-2">
                <div class="grid-x grid-margin-x">
                    @foreach ($news as $item)
                        <div class="cell small-12 medium-3 margin-bottom-1 text-center medium-text-left">
                            <a href="{{ route('news.view', [$item->slug]) }}" title="{{ $item->title }}">
                                {!! Html::image(route('imagecache', ['medium', $item->image]), null, ['alt' => $item->title, 'title' => $item->title]) !!}
                            </a>
                        </div>
                        <div class="cell small-12 medium-9 margin-bottom-1">
                            <a href="{{ route('news.view', [$item->slug]) }}" title="{{ $item->title }}">
                                {{ $item->title }}
                            </a>
                            <p>{{ str_limit($item->text, 400) }}</p>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
    </article>
@endsection