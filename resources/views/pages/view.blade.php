@extends('layouts/general')

@section('content')
    @if($page)
        <div class="grid-container margin-top-2 hide-for-small-only">
            <div class="grid-x grid-margin-x">
                <div class="cell">
                    <nav role="navigation">
                        {!! Breadcrumbs::render('page', $page) !!}
                    </nav>
                </div>
            </div>
        </div>
    @endif
    <div class="grid-container margin-vertical-3">
        <div class="grid-x grid-margin-x">
            <div class="cell medium-3 small-12 padding-top-3 hide-for-small-only">
                @include('include.article-sidebar')
            </div>
            <div class="cell medium-9 small-12">
                @if($page)
                    <article>
                        <h1 class="text-center">{{ $page->title }}</h1>
                        {!! $page->text !!}
                    </article>
                @endif
            </div>
        </div>
    </div>
@endsection