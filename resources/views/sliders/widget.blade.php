@if (!$sliders->isEmpty())
    <section id="slider" class="margin-bottom-2 vue-carousel-pagination-inner">
        <div class="grid-container">
            <div class="grid-x">
                <carousel :per-page="1" :loop="true" :navigation-enabled="true" :autoplayLoop="true" :autoplay="false">
                    @foreach ($sliders as $slide)
                        <slide>
                            @if ($slide->url)
                                <a href="{{ $slide->url }}" title="{{ $slide->title }}" target="_blank">
                                    <img src="{{ $slide->image }}" alt="{{ $slide->title }}" title="{{ $slide->title }}" />
                                </a>
                            @else
                                <img src="{{ $slide->image }}" alt="{{ $slide->title }}" title="{{ $slide->title }}" />
                            @endif
                        </slide>
                    @endforeach
                </carousel>
            </div>
        </div>
    </section>
@endif