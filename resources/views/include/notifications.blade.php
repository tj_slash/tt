@if (session()->has('flash_notification.message'))
    <notifications
        message="{{ session('flash_notification.message') }}"
        level="{{ session('flash_notification.level') }}"
    >
    </notifications>
@endif
@if (count($errors) > 0)
    @foreach ($errors->all() as $error)
        <notifications message="{{ $error }}" level="error"></notifications>
    @endforeach
@endif