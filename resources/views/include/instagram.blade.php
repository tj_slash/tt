<section id="instagramm" class="margin-bottom-2">
    <div class="title-line">
        <span>Наш Instagram</span>
    </div>
    <div class="hide-for-small-only">
        <carousel :per-page-custom="[[480, 1], [768, 4], [1024, 6], [1824, 8]]" :loop="true" :autoplayLoop="true" :autoplay="false" :pagination-enabled="true">
            @foreach ($photos as $photo)
                <slide>
                    <a href="{{ $photo->link }}" target="_blank" title="">
                        <img src="{{ $photo->images->low_resolution->url }}" alt="" title="" />
                    </a>
                </slide>
            @endforeach
        </carousel>
        <div class="clearfix"></div>
    </div>
    <div class="show-for-small-only">
        <div class="VueCarousel-inner align-center">
            <div class="VueCarousel-slide">
                <a href="{{ $photo->link }}" target="_blank" title="">
                    <img src="{{ $photo->images->low_resolution->url }}" alt="" title="" />
                </a>
            </div>
        </div>
    </div>
</section>