<aside>
    <nav>
        <ul class="vertical menu">
            <li><a href="{{ route('pages.view', ['about']) }}" title="О магазине">О магазине</a></li>
            <li><a href="{{ route('pages.view', ['info']) }}" title="Информация для покупателей">Информация для покупателей</a></li>
            <li><a href="{{ route('news') }}" title="Новости и статьи">Новости и статьи</a></li>
            <li><a href="{{ route('photoreports') }}" title="Фоторепортажи">Фоторепортажи</a></li>
            <li><a href="{{ route('pages.view', ['history']) }}" title="История">История</a></li>
        </ul>
    </nav>
</aside>