<div class="title-line margin-bottom-2"></div>
<section id="contacs" class="margin-bottom-2">
    <div class="grid-container">
        <div class="grid-x grid-margin-x">
            <div class="cell small-12">
                <h5 class="text-center">Наши контакты</h5>
                <ul class="margin-0">
                    <li class="display-inline-block margin-right-2">
                        <address>
                            <i class="mdi mdi-map-marker"></i> г. Благовещенск, ул. Амурская, 170
                        </address>
                    </li>
                    <li class="display-inline-block margin-right-2">
                        <a href="tel:+74162443000" target="_blank">
                            <i class="mdi mdi-phone"></i> +7 (4162) 44-30-00
                        </a>
                    </li>
                    <li class="display-inline-block margin-right-2">
                        <a href="tel:+74162443030" target="_blank">
                            <i class="mdi mdi-phone"></i> +7 (4162) 44-30-30
                        </a>
                    </li>
                    <li class="display-inline-block margin-right-2">
                        <a href="tel:+74162445050" target="_blank">
                            <i class="mdi mdi-phone"></i> +7 (4162) 44-50-50
                        </a>
                    </li>
                    <li class="display-inline-block">
                        <a href="mailto:info@tt-store.ru" target="_blank">
                            <i class="mdi mdi-mail-ru"></i> info@tt-store.ru
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<footer>
	<div class="grid-container">
		&copy; {{ date('Y') == 2017 ? date('Y') : date('2017 - Y') }} | TT-STORE.RU
	</div>
</footer>
<top></top>