<aside>
    <nav>
        <ul class="vertical menu accordion-menu" data-accordion-menu>
            <li>
                <a href="#">Бренд</a>
                <ul class="menu vertical nested">
                    <li><a href="#">Женское</a></li>
                    <li><a href="#">Мужское</a></li>
                    <li><a href="#">Косметика</a></li>
                    <li><a href="#">Украшения</a></li>
                    <li><a href="#">Часы</a></li>
                </ul>
            </li>
            <li>
                <a href="#">Дополнительно</a>
                <ul class="menu vertical nested">
                    <li><a href="#">Женское</a></li>
                    <li><a href="#">Мужское</a></li>
                    <li><a href="#">Косметика</a></li>
                    <li><a href="#">Украшения</a></li>
                    <li><a href="#">Часы</a></li>
                </ul>
            </li>
            <li>
                <a href="#">Выбрать цвет</a>
                <ul class="menu vertical nested">
                    <li><a href="#">Женское</a></li>
                    <li><a href="#">Мужское</a></li>
                    <li><a href="#">Косметика</a></li>
                    <li><a href="#">Украшения</a></li>
                    <li><a href="#">Часы</a></li>
                </ul>
            </li>
            <li>
                <a href="#">Выбрать размер</a>
                <ul class="menu vertical nested">
                    <li><a href="#">Женское</a></li>
                    <li><a href="#">Мужское</a></li>
                    <li><a href="#">Косметика</a></li>
                    <li><a href="#">Украшения</a></li>
                    <li><a href="#">Часы</a></li>
                </ul>
            </li>
        </ul>
    </nav>
</aside>