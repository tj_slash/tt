<header>
	<div class="grid-container hide-for-small-only">
		<div class="grid-x grid-margin-x margin-top-1">
			<div class="cell medium-4 small-12 align-middle flex-container">
				<div class="grid-x">
					<div class="cell medium-6 small-12">
						<form action="{{ route('search') }}" method="GET">
							<div class="float-left width-75">
								<input type="text" name="q" placeholder="Я ищу..." class="margin-0">
							</div>
							<div class="float-left width-25">
								<button type="submit"><img src="/images/search.png" /></button>
							</div>
						</form>
					</div>
					<div class="cell medium-6 small-12 padding-left-2 align-middle flex-container">
						<a href="{{ route('contacts') }}" title="Контакты" class="hover-dashed">Контакты</a>
					</div>
				</div>
			</div>
			<div class="cell medium-4 small-12 sitename align-center">
				<a href="{{ route('home') }}">
					<span>
						T.T. магазин
					</span>
				</a>
			</div>
			<div class="cell medium-4 small-12 align-middle flex-container">
				<div class="grid-x width-100">
					<div class="cell small-12 text-right margin-bottom-1">
						@if (!Auth::check())
							<a class="hover-dashed" title="Личный кабинет" data-open="signinModal">
								Личный кабинет
							</a>
						@else
							<a class="hover-dashed" title="Выйти" href="/logout">
								Выйти
							</a>
						@endif
					</div>
					<div class="cell small-6 text-right">
						<a href="https://instagram.com/tt_store_blg" target="_blank" class="black"><i class="mdi mdi-instagram"></i> tt_store_blg</a>
					</div>
					<div class="cell small-6 text-right text-uppercase">
						<a href="{{ route('look-book') }}" class="look-book-link">
							<img src="/images/lookbook.png" /><span>Look Book</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="grid-container" id="mobile-sticky-header">
		<div class="align-right sitename show-for-small-only">
			<a href="{{ route('home') }}">
				<span>
					T.T. магазин
				</span>
			</a>
		</div>
		<div class="grid-x grid-margin-x">
			<div class="cell small-12">
				{!! App\Http\Controllers\MainController::menu() !!}
			</div>
		</div>
	</div>
    <div class="show-for-small-only margin-vertical-3 padding-vertical-1"></div>
</header>
@if (!Auth::check())
	<div class="reveal" id="signinModal" data-reveal>
		<h1 class="text-center">Вход</h1>
		<form method="POST" action="{{ route('login') }}">
			{{ csrf_field() }}
			<input type="email" placeholder="Введите ваш электронный адрес" class="margin-bottom-1"  name="email" value="{{ old('email') }}" required autofocus>
			@if ($errors->has('email'))
				<span class="form-error is-visible">
	                <strong>{{ $errors->first('email') }}</strong>
	            </span>
			@endif
			<input type="password" placeholder="Введите ваш пароль" class="margin-bottom-1" name="password" required>
			@if ($errors->has('password'))
				<span class="form-error is-visible">
	                <strong>{{ $errors->first('password') }}</strong>
	            </span>
			@endif
			<div class="margin-bottom-1 medium-text-right">
				<a data-open="forgotModal">Забыли пароль?</a>
			</div>
			<div class="text-center">
				<button type="submit" class="text-uppercase button black margin-bottom-1 text-center">
					Войти
				</button>
			</div>
			<div class="text-center">
				<a data-open="signupModal">Регистрация</a>
			</div>
		</form>
		<button class="close-button" data-close aria-label="Закрыть" type="button">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	<div class="reveal" id="signupModal" data-reveal>
		<h1 class="text-center">Регистрация</h1>
		<form method="POST" action="{{ route('register') }}">
			{{ csrf_field() }}
			<input type="text" placeholder="Введите Имя" class="margin-bottom-1" name="name" value="{{ old('name') }}" required>
			@if ($errors->has('name'))
				<span class="form-error is-visible">
	                <strong>{{ $errors->first('name') }}</strong>
	            </span>
			@endif
			<div class="grid-x margin-bottom-1">
				<div class="cell small-12 medium-6">
					<input type="radio" value="{{ \App\Models\Users::FEMALE }}" id="sex-female" name="sex" checked="checked" class="custom">
					<label for="sex-female" class="radio-label">Женщина</label>
				</div>
				<div class="cell small-12 medium-6">
					<input type="radio" value="{{ \App\Models\Users::MALE }}" id="sex-male" name="sex" class="custom">
					<label for="sex-male" class="radio-label">Мужчина</label>
				</div>
			</div>
			<input type="text" placeholder="Введите дату рождения" name="birthday" class="margin-bottom-1" value="{{ old('birthday') }}">
			<input type="email" placeholder="Введите ваш электронный адрес" class="margin-bottom-1" name="email" value="{{ old('email') }}" required>
			@if ($errors->has('email'))
				<span class="form-error is-visible">
	                <strong>{{ $errors->first('email') }}</strong>
	            </span>
			@endif
			<input type="text" placeholder="Введите ваш телефон" class="margin-bottom-1" name="phone" value="{{ old('phone') }}">
			<input type="password" placeholder="Придумайте пароль" class="margin-bottom-1" name="password" required>
			@if ($errors->has('password'))
				<span class="form-error is-visible">
	                <strong>{{ $errors->first('password') }}</strong>
	            </span>
			@endif
			<input type="password" class="margin-bottom-1" placeholder="Повторите пароль" name="password_confirmation" required>
			<div class="margin-bottom-1 flex-container">
				<div>
					<input type="checkbox" id="notify" name="notify" checked value="1" class="custom" />
					<label for="notify" class="checkbox-label" style="margin-top:5px!important"></label>
				</div>
				<div>
					Если вы не хотите раньше остальных узнавать о новинках и специальных предложениях по почте, снимите эту галочку
				</div>
			</div>
			<div class="text-center">
				<button type="submit" class="text-uppercase button black margin-bottom-1 text-center">
					Зарегистрироваться
				</button>
			</div>
			<div class="text-center margin-bottom-1">
				<a data-open="signinModal">Войти</a>
			</div>
			<div class="text-center">
				Нажимая кнопку &laquo;Зарегистироваться&raquo;, вы принимаете настоящие <a>&laquo;Правила продажи товаров&raquo;</a> и соглашаетесь на обработку персональных данных, Мы гарантируем, что ваши данные не будут переданы третьим лицам
			</div>
		</form>
		<button class="close-button" data-close aria-label="Закрыть" type="button">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	<div class="reveal" id="forgotModal" data-reveal>
		<h1 class="text-center">Восстановление пароля</h1>
		<form method="POST" action="{{ route('password.email') }}">
			{{ csrf_field() }}
			<input type="email" placeholder="Введите ваш электронный адрес" class="margin-bottom-1" name="email" value="{{ old('email') }}" required>
			@if ($errors->has('email'))
				<span class="form-error is-visible">
	                <strong>{{ $errors->first('email') }}</strong>
	            </span>
			@endif
			<div class="text-center">
				<button type="submit" class="text-uppercase button black margin-bottom-1 text-center">
					Восстановить пароль
				</button>
			</div>
			<div class="text-center margin-bottom-1">
				<a data-open="signinModal">Войти</a>
			</div>
		</form>
		<button class="close-button" data-close aria-label="Закрыть" type="button">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
@endif