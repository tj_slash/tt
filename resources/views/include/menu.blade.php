<nav class="hide-for-small-only">
	<ul class="menu align-center">
		<li><a href="{{ route('brands') }}" title="Бренды" class="{{{ (Request::is('brands') ? 'active' : '') }}}">Бренды</a></li>
		@if (!$categories->isEmpty())
			@foreach ($categories as $category)
				<li>
					<a href="{{ route('catalog.category.view', [$category->slug]) }}" title="{{ $category->title }}"  class="{{{ (Request::is('catalog/category/' . $category->slug) ? 'active' : '') }}}">
						{{ $category->title }}
					</a>
				</li>
			@endforeach
		@endif
		<li><a href="{{ route('shares') }}" title="Акции" class="{{{ (Request::is('shares') ? 'active' : '') }}}">Акции</a></li>
		<li><a href="{{ route('pages.view', ['about']) }}" title="О магазине" class="{{{ (Request::is('pages/about') ? 'active' : '') }}}">О магазине</a></li>
	</ul>
</nav>
<div class="mobile-menu-bar" data-responsive-toggle="mobile-menu" data-hide-for="medium">
	<button class="menu-icon" type="button" data-toggle="mobile-menu"></button>
</div>
<div class="show-for-small-only" id="mobile-menu" style="display: none">
	<nav class="mobile-menu">
		{{--<div class="mobile-dashboard">--}}
		{{--@if (!Auth::check())--}}
			{{--<a title="Личный кабинет" data-open="signinModal">--}}
				{{--Личный кабинет--}}
			{{--</a>--}}
		{{--@else--}}
			{{--<a title="Выйти" href="/logout">--}}
				{{--Выйти--}}
			{{--</a>--}}
		{{--@endif--}}
		{{--</div>--}}
		<ul class="vertical menu drilldown" data-auto-height="true" data-drilldown data-parent-link='true' data-back-button="<li class='js-drilldown-back'><a>Назад</a></li>">
			<li class="padding-vertical-0">
				<div class="text-left text-uppercase">
					<a href="{{ route('look-book') }}" class="look-book-link {{{ (Route::is('look-book') ? 'active' : '') }}}">
						<span>Look Book</span><img src="/images/lookbook.png" />
					</a>
				</div>
			</li>
			<li><a href="{{ route('catalog.newest.view') }}" title="Новинки" class="{{{ (Route::is('catalog.newest.view') ? 'active' : '') }}}"><span>Новинки</span></a></li>
			<li><a href="{{ route('brands') }}" title="Бренды" class="{{{ (Request::is('brands') ? 'active' : '') }}}"><span>Бренды</span></a></li>
			@if (!$categories->isEmpty())
				@foreach ($categories as $category)
					{!! renderCatalogNode($category) !!}
				@endforeach
			@endif
			<li><a href="{{ route('shares') }}" title="Акции" class="{{{ (Request::is('shares') ? 'active' : '') }}}"><span>Акции</span></a></li>
			<li>
                <a href="{{ route('pages.view', ['about']) }}" title="О магазине" class="{{{ (Request::is('pages/about') ? 'active' : '') }}}"><span>О магазине</span></a>
                <ul class="menu vertical nested">
                    <li><a href="{{ route('pages.view', ['info']) }}" title="Информация для покупателей" class="{{{ (Request::is('pages/info') ? 'active' : '') }}}"><span>Информация для покупателей</span></a></li>
                    <li><a href="{{ route('news') }}" title="Новости и статьи" class="{{{ (Route::is('news') ? 'active' : '') }}}"><span>Новости и статьи</span></a></li>
                    <li><a href="{{ route('photoreports') }}" title="Фоторепортажи" class="{{{ (Route::is('photoreports') ? 'active' : '') }}}"><span>Фоторепортажи</span></a></li>
                    <li><a href="{{ route('pages.view', ['history']) }}" title="История" class="{{{ (Request::is('pages/history') ? 'active' : '') }}}"><span>История</span></a></li>
                </ul>
            </li>
		</ul>
		<a href="tel:+74162443000" class="call-button">
			<img src="/images/smartphone-with-a-smile.svg" /><span>+7 (4162) 44-30-00</span>
		</a>
        <div class="mobile-menu-close" data-responsive-toggle="mobile-menu" data-hide-for="medium">
            <button aria-label="Закрыть" type="button" class="close-button" data-toggle="mobile-menu"><span aria-hidden="true">×</span></button>
        </div>
	</nav>
</div>