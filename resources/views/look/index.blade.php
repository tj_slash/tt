@extends('layouts/general')

@section('content')
    <div class="grid-container margin-top-2 hide-for-small-only">
        <div class="grid-x grid-margin-x">
            <div class="cell">
                <nav role="navigation">
                    {!! Breadcrumbs::render('look-book') !!}
                </nav>
            </div>
        </div>
    </div>
    <div class="grid-container margin-bottom-3 margin-top-3" id="look-book">
        <article class="margin-bottom-3">
            <h1 class="font-italic text-center">Look Book</h1>
            @if (!empty($page))
                <p class="hide-for-small-only">
                    {!! $page->text !!}
                </p>
            @endif
            <h2 class="text-center hide-for-small-only">{{ $look->title }}</h2>
            <p class="hide-for-small-only">{{ $look->text }}</p>
        </article>
        @if($look)
            <look inline-template selected="{{ $look->slug }}">
                <div class="grid-x">
                    <div class="cell small-12 medium-3 show-for-small-only margin-bottom-1">
                        <form action="" method="POST">
                            <select v-model="slug" v-on:change="changeLook()">
                                @foreach ($looks as $option)
                                    <option value="{{ $option->slug }}">{{ $option->title }}</option>
                                @endforeach
                            </select>
                        </form>
                    </div>

                    <div class="cell small-12 medium-6">
                        <div class="grid-container vue-carousel-pagination-inner">
                            <carousel ref="carousel" :per-page="1" :loop="true" :autoplayLoop="true" :autoplay="false"
                                      :navigation-enabled="true" :pagination-enabled="true">
                                @foreach ($look->images as $image)
                                    <slide>
                                        <div class="text-center">
                                            {!! Html::image(route('imagecache', ['product_big', $image]), null, ['alt' => $look->title, 'title' => $look->title]) !!}
                                        </div>
                                    </slide>
                                @endforeach
                            </carousel>
                        </div>
                    </div>
                    <div class="cell small-12 medium-3 show-for-small-only">
                        <article>
                            <h4 class="text-center">{{ $look->title }}</h4>
                            <p>{{ $look->text }}</p>
                        </article>
                    </div>
                    <div class="cell hide-for-small-only medium-3 text-center">
                        @if(count($look->images) > 4)
                            <div id="scroll-up" class="arrow-up"></div>
                        @endif
                        <div class="thumbs" id="scroll">
                            @foreach ($look->images as $i => $image)
                                <a @click="changePageCarousel({{ $i }})">
                                    {!! Html::image(route('imagecache', ['product_small_carousel', $image]), null, ['alt' => $look->title, 'title' => $look->title]) !!}
                                </a>
                            @endforeach
                        </div>
                        @if(count($look->images) > 4)
                            <div id="scroll-down" class="arrow-down"></div>
                        @endif
                    </div>
                    <div class="cell small-12 medium-3 hide-for-small-only">
                        <form action="" method="POST">
                            <select v-model="slug" v-on:change="changeLook()">
                                @foreach ($looks as $option)
                                    <option value="{{ $option->slug }}">{{ $option->title }}</option>
                                @endforeach
                            </select>
                        </form>
                    </div>
                </div>
            </look>
            <div class="grid-container">
                <section id="novelties" class="margin-top-3">
                    <h5 class="font-italic text-center">Детали образа</h5>
                    <div class="grid-x">
                        <carousel :per-page-custom="[[320, 2], [768, 3], [1024, 4], [1824, 4]]" :loop="true"
                                  :autoplayLoop="true" :autoplay="false" :navigation-enabled="true"
                                  :pagination-enabled="true">
                            @foreach ($look->products as $product)
                                <slide>
                                    <a href="{{ route('catalog.view', [$product->slug]) }}"
                                       title="{{ $product->title }}"
                                       class="item text-center margin-bottom-1 flex-container flex-dir-column">
                                        {!! Html::image(route('imagecache', ['product_widget', $product->mainImage]), null, ['alt' => $product->title, 'title' => $product->title, 'width' => '165', 'height' => '240']) !!}
                                        <h6 class="margin-top-1">{{ $product->title }}</h6>
                                        <p>{{ $product->anonce }}</p>
                                        <span class="price">{{ $product->price }} <i
                                                    class="mdi mdi-currency-rub"></i></span>
                                    </a>
                                </slide>
                            @endforeach
                        </carousel>
                    </div>
                </section>
                <div class="cell small-12 text-center margin-top-1">
                    <a class="button hollow black" href="{{ route('catalog.look.view', [$look->slug]) }}"
                       title="Все детали образа">
                        Все детали образа
                    </a>
                </div>
            </div>
        @else
            <article class="margin-bottom-3">
                <h1 class="font-italic text-center">Look Book</h1>
                <p class="text-center">Еще не добавлен ни один Look Book</p>
            </article>
        @endif
    </div>
@endsection