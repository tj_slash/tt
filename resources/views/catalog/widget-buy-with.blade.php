@if (!$products->isEmpty())
    <section id="novelties" class="margin-vertical-2">
        <h5 class="font-italic text-center">С этим товаром часто смотрят</h5>
        <div class="grid-container">
            <div class="grid-x">
                <carousel :per-page-custom="[[375, 2], [768, 3], [1024, 4], [1824, 5]]" :loop="true" :autoplayLoop="true" :autoplay="false" :navigation-enabled="true" :pagination-enabled="true">
                    @foreach ($products as $product)
                        <slide>
                            <a href="{{ route('catalog.view', [$product->slug]) }}" title="{{ $product->title }}" class="item margin-bottom-1 text-center flex-container flex-dir-column">
                                {!! Html::image(route('imagecache', ['product_widget', $product->mainImage]), null, ['alt' => $product->title, 'title' => $product->title]) !!}
                                <h6 class="margin-top-1">{{ $product->title }}</h6>
                                <p>{{ $product->anonce }}</p>
                                <div class="clear clearfix"></div>
                                <span class="price">{{ $product->price }} <i class="mdi mdi-currency-rub"></i></span>
                            </a>
                        </slide>
                    @endforeach
                </carousel>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
@endif