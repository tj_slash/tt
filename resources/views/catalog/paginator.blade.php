@if ($paginator->hasPages())
    <ul class="pagination text-center margin-top-2">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="disabled">&#9666;</li>
        @else
            <li><a href="{{ $paginator->previousPageUrl() }}" rel="prev">&#9666;</a></li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="ellipsis"></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="current">{{ $page }}</li>
                    @else
                        <li><a href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li><a href="{{ $paginator->nextPageUrl() }}" rel="next">&#9656;</a></li>
        @else
            <li class="disabled">&#9656;</li>
        @endif
    </ul>
@endif
