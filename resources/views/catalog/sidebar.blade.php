<aside>
    <nav>
        <ul class="vertical menu accordion-menu" data-accordion-menu>
            <a class="reset" v-show="brands_id.length > 0" @click="resetBrands()">Сбросить</a>
            <li>
                <a>Бренд</a>
                <ul class="menu vertical nested">
                    <input type="text" v-model="searchBrand" placeholder="Поиск по брендам">
                    <div class="overflow-y-scroll padding-top-1" style="height:200px">
                        <li v-for="brand in filterBrands">
                            <label class="checkbox-label" v-bind:class="{ checked: brands_id.includes(brand.id) }">
                                <input type="checkbox" v-model="brands_id" v-bind:value="brand.id" v-on:change="changeSort()"> @{{ brand.title }}
                            </label>
                        </li>
                    </div>
                </ul>
            </li>
        </ul>
        @if($categories)
            <ul class="menu vertical without-lines">
                @foreach ($categories as $category)
                    <li>
                        <a href="{{ route('catalog.category.view', $category->slug) }}" title="{{ $category->title }}">
                            {{ $category->title }}
                        </a>
                    </li>
                @endforeach
            </ul>
        @endif
        <ul class="vertical menu accordion-menu" data-accordion-menu>
            <a class="reset" v-show="colors_id.length > 0" @click="resetColors()">Сбросить</a>
            <li>
                <a>Выбрать цвет</a>
                <ul class="menu vertical nested">
                    <div class="overflow-y-scroll padding-top-1" style="height:200px">
                        <li v-for="color in colors">
                            <label class="checkbox-label" v-bind:class="{ checked: colors_id.includes(color.id) }">
                                <input type="checkbox" v-model="colors_id" v-bind:id="color.id" v-bind:value="color.id" v-on:change="changeSort()"> @{{ color.title }}
                            </label>
                        </li>
                    </div>
                </ul>
            </li>
        </ul>
        <ul class="vertical menu accordion-menu" data-accordion-menu>
            <a class="reset" v-show="sizes_id.length > 0" @click="resetSizes()">Сбросить</a>
            <li>
                <a>Выбрать размер</a>
                <ul class="menu vertical nested">
                    <div class="overflow-y-scroll padding-top-1" style="height:200px">
                        <li v-for="size in sizes">
                            <label class="checkbox-label" v-bind:class="{ checked: sizes_id.includes(size.id) }">
                                <input type="checkbox" v-model="sizes_id" v-bind:id="size.id" v-bind:value="size.id" v-on:change="changeSort()"> @{{ size.title }}
                            </label>
                        </li>
                    </div>
                </ul>
            </li>
        </ul>
        <ul class="menu vertical without-lines">
            <li class="padding-1">
                <label class="checkbox-label " :class="{ checked: withDiscount }">
                    <input type="checkbox" v-model="withDiscount" @change="changeSort()"> Только со скидкой
                </label>
            </li>
        </ul>
    </nav>
</aside>