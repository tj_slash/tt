@extends('layouts/general')

@section('content')
    <div class="grid-container margin-top-2 hide-for-small-only">
        <div class="grid-x grid-margin-x">
            <div class="cell">
                <nav role="navigation">
                    @if($category)
                        {!! Breadcrumbs::render('catalog-category', $category) !!}
                    @elseif($brand)
                        {!! Breadcrumbs::render('catalog-brand', $brand) !!}
                    @else
                        {!! Breadcrumbs::render('catalog') !!}
                    @endif
                </nav>
            </div>
        </div>
    </div>
    <catalog-search inline-template category_slug="{{ !empty($category) ? $category->slug : false }}"
                    colors_json="{{ $colors }}" brands_json="{{ $brands }}" sizes_json="{{ $sizes }}"
                    brand_id="{{ !empty($brand) ? $brand->id : false }}"
                    look_id="{{ !empty($look) ? $look->id : false }}"
                    sorting_id="{{ !empty($sorting) ? $sorting : 1 }}">
        <div class="grid-container margin-bottom-3 margin-top-3" id="catalog">
            <h1 class="text-center">Каталог</h1>

            <div class="grid-x grid-margin-x">
                <div class="cell medium-offset-2 medium-8 text-center  hide-for-small-only">
                    <span>@{{count}} @{{count | plural(['товар', 'товара', 'товаров'])}}</span>
                </div>
                <div class="cell medium-2 small-6 show-for-small-only">
                    <select data-open="modal-filter">
                        <option>
                            Фильтры
                        </option>
                    </select>
                </div>
                <div class="cell medium-2 small-6">
                    <select v-model="sorting" v-on:change="changeSort()">
                        <option v-bind:value="sort.value" v-for="sort in sorts">
                            @{{ sort.name }}
                        </option>
                    </select>
                </div>
                <div class="reveal" id="modal-filter" data-reveal data-multiple-opened="true">
                    {!! App\Http\Controllers\CatalogController::sidebar($category, true) !!}
                    <button class="close-button" data-close aria-label="Закрыть" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="margin-top-1 text-center">
                        <button @click="closeAllModals" class="button hollow black">@{{count}} @{{count |
                            plural(['товар', 'товара', 'товаров'])}}
                        </button>
                    </div>
                </div>
            </div>
            <div class="grid-x grid-margin-x show-for-small-only">
                <div class="cell medium-2 small-6 text-right">
                    <span>@{{count}} @{{count | plural(['товар', 'товара', 'товаров'])}}</span>
                </div>
                <div class="cell medium-2 small-6 small-6">
                    <a @click="resetAll" class="hover-dashed black">Очистить все</a>
                </div>
            </div>

            <div class="grid-x grid-margin-x">
                <div class="cell medium-3 small-12 padding-top-3 hide-for-small-only">
                    {!! App\Http\Controllers\CatalogController::sidebar($category) !!}
                </div>
                <div class="cell medium-9 small-12">
                    <div class="grid-x">
                        <div class="cell large-4 medium-4 small-6 text-center margin-bottom-2"
                             v-for="product in products">
                            <div class="item padding-1">
                                <a v-bind:href="product.url" v-bind:title="product.title">
                                    <img v-bind:src="product.image" v-bind:alt="product.title"
                                         v-bind:title="product.title"/>
                                    <h3>@{{ product.title }}</h3>
                                    <span>@{{ product.anonce }}</span>
                                    <div v-if="product.discount_price">
                                        <span class="old_price">
                                            @{{ product.price }} <i class="mdi mdi-currency-rub"></i>
                                        </span>
                                        <span class="discount_price">
                                            @{{ product.discount_price }} <i class="mdi mdi-currency-rub"></i>
                                        </span>
                                    </div>
                                    <span v-else class="price">
                                        @{{ product.price }} <i class="mdi mdi-currency-rub"></i>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="grid-x">
                        <div class="cell small-12 text-center">
                            <a class="button hollow black"
                               v-if="this.count > this.limit && this.count != this.loaded && this.count > 0"
                               title="Смотреть еще" @click="showMore(this)">Смотреть еще</a>
                        </div>
                    </div>
                    {{--<ul class="pagination text-center margin-top-2"--}}
                        {{--v-if="this.count > this.limit && this.count != this.loaded && this.count > 0">--}}
                        {{-- Previous Page Link --}}
                        {{--<li v-if="loaded == limit" class="disabled">&#9666;</li>--}}
                        {{--<li v-else><a>&#9666;</a></li>--}}
                        {{-- Page numbers --}}
                        {{--<li v-for="n in Math.ceil(count/limit)"--}}
                            {{--:class="{current: n == Math.ceil(loaded/limit)}">--}}
                            {{--<template v-if="n == Math.ceil(loaded/limit)">--}}
                                {{--@{{ n }}--}}
                            {{--</template>--}}
                            {{--<a v-else>@{{ n }}</a>--}}
                        {{--</li>--}}
                        {{-- Next Page Link --}}
                        {{--<li v-if="loaded == count" class="disabled">&#9656;</li>--}}
                        {{--<li v-else><a>&#9656;</a></li>--}}
                    {{--</ul>--}}
                </div>
            </div>
        </div>
    </catalog-search>
@endsection