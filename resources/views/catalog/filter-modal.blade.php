<h1 class="text-center">Фильтры</h1>
<ul class="vertical menu modal-filter-list">
    <li>
        <a data-open="brands-filter" data-multiple-opened="true">Бренд</a>
    </li>
    @if($categories)
        <li>
            <a data-open="categories-filter" data-multiple-opened="true">Категории</a>
        </li>
    @endif
    <li>
        <a data-open="sizes-filter" data-multiple-opened="true">Размеры</a>
    </li>
    <li>
        <a data-open="colors-filter" data-multiple-opened="true">Цвета</a>
    </li>
</ul>

<div id="brands-filter" class="reveal" data-reveal data-multiple-opened="true">
    <h1 class="text-center">Бренд</h1>
    <ul class="vertical menu modal-filter-list">
        <a class="reset" v-show="brands_id.length > 0" @click="resetBrands()">Сбросить</a>
        <li>
            <input type="text" v-model="searchBrand" placeholder="Поиск по брендам">
            <ul class="menu vertical nested">
                <li v-for="brand in filterBrands">
                    <label class="checkbox-label" v-bind:class="{ checked: brands_id.includes(brand.id) }">
                        <input type="checkbox" v-model="brands_id" v-bind:value="brand.id" v-on:change="changeSort()">
                        @{{ brand.title }}
                    </label>
                </li>
            </ul>
        </li>
    </ul>
    <button class="close-button" data-close aria-label="Закрыть" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="margin-top-1 text-center">
        <button @click="closeAllModals" class="button hollow black">@{{count}} @{{count | plural(['товар', 'товара',
            'товаров'])}}
        </button>
    </div>
</div>
@if($categories)
    <div id="categories-filter" class="reveal" data-reveal data-multiple-opened="true">
        <h1 class="text-center">Категория</h1>
        <ul class="menu vertical modal-filter-list">
            <li>
                <ul class="menu vertical nested">
                    @foreach ($categories as $category)
                        <li>
                            <a href="{{ route('catalog.category.view', $category->slug) }}"
                               title="{{ $category->title }}">
                                {{ $category->title }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </li>
        </ul>
        <button class="close-button" data-close aria-label="Закрыть" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="margin-top-1 text-center">
            <button @click="closeAllModals" class="button hollow black">@{{count}} @{{count | plural(['товар', 'товара',
                'товаров'])}}
            </button>
        </div>
    </div>
@endif
<div id="sizes-filter" class="reveal" data-reveal data-multiple-opened="true">
    <h1 class="text-center">Выбрать размер</h1>
    <ul class="vertical menu modal-filter-list">
        <a class="reset" v-show="sizes_id.length > 0" @click="resetSizes()">Сбросить</a>
        <li>
            <ul class="menu vertical nested">
                <li v-for="size in sizes">
                    <label class="checkbox-label" v-bind:class="{ checked: sizes_id.includes(size.id) }">
                        <input type="checkbox" v-model="sizes_id" v-bind:id="size.id" v-bind:value="size.id"
                               v-on:change="changeSort()"> @{{ size.title }}
                    </label>
                </li>
            </ul>
        </li>
    </ul>
    <button class="close-button" data-close aria-label="Закрыть" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="margin-top-1 text-center">
        <button @click="closeAllModals" class="button hollow black">@{{count}} @{{count | plural(['товар', 'товара',
            'товаров'])}}
        </button>
    </div>
</div>
<div id="colors-filter" class="reveal" data-reveal data-multiple-opened="true">
    <h1 class="text-center">Выбрать цвет</h1>
    <ul class="vertical menu modal-filter-list">
        <a class="reset" v-show="colors_id.length > 0" @click="resetColors()">Сбросить</a>
        <li>
            <ul class="menu vertical nested">
                <li v-for="color in colors">
                    <label class="checkbox-label" v-bind:class="{ checked: colors_id.includes(color.id) }">
                        <input type="checkbox" v-model="colors_id" v-bind:id="color.id" v-bind:value="color.id"
                               v-on:change="changeSort()"> @{{ color.title }}
                    </label>
                </li>
            </ul>
        </li>
    </ul>
    <button class="close-button" data-close aria-label="Закрыть" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="margin-top-1 text-center">
        <button @click="closeAllModals" class="button hollow black">@{{count}} @{{count | plural(['товар', 'товара',
            'товаров'])}}
        </button>
    </div>
</div>