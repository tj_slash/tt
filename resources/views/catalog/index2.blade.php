@extends('layouts/general')

@section('content')
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x">
            <div class="cell">
                <nav role="navigation">
                    @if($category)
                        {!! Breadcrumbs::render('catalog-category', $category) !!}
                    @elseif($brand)
                        {!! Breadcrumbs::render('catalog-brand', $brand) !!}
                    @else
                        {!! Breadcrumbs::render('catalog') !!}
                    @endif
                </nav>
            </div>
        </div>
    </div>
    <div class="grid-container margin-bottom-3 margin-top-3" id="catalog">
        <h1 class="text-center">Каталог</h1>

        <div class="grid-x grid-margin-x">
            <div class="cell medium-offset-2 medium-8 small-12 text-center">
                {{ $count }} товаров
            </div>
            <div class="cell medium-2 small-12">
                <select>
                    <option value="">На ваш вкус</option>
                    <option value="">По новизне</option>
                    <option value="">По популярности</option>
                    <option value="">По цене -</option>
                    <option value="">По цене +</option>
                </select>
            </div>
        </div>

        <div class="grid-x grid-margin-x">
            <div class="cell medium-3 small-12 padding-top-3">
                {!! App\Http\Controllers\CatalogController::sidebar() !!}
            </div>
            <div class="cell medium-9 small-12">
                <div class="grid-x">
                    @foreach ($products as $product)
                        <div class="cell large-4 medium-4 small-12 item text-center margin-bottom-2 padding-horizontal-3 padding-vertical-1">
                            <a href="{{ route('catalog.view', [$product->slug]) }}" title="{{ $product->title }}">
                                {!! Html::image(route('imagecache', ['product_list', $product->mainImage]), null, ['alt' => $product->title, 'title' => $product->title]) !!}
                                <h3>{{ $product->title }}</h3>
                                <span>{{ $product->anonce }}</span>
                                <span>{{ $product->price }} &#x20bd;</span>
                            </a>
                        </div>
                    @endforeach
                </div>
                <div class="grid-x">
                    <div class="cell small-12 text-center">
                        <a class="button hollow black" href="/news" title="Смотреть еще">Смотреть еще</a>
                    </div>
                </div>
                {{ $products->links('catalog.paginator') }}
            </div>
        </div>
    </div>
@endsection