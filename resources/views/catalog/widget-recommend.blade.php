@if (!$products->isEmpty())
    <section id="recommend-professionals" class="margin-bottom-2">
        <div class="title-line without-line">
            <span class="font-italic">Рекомендуют профессионалы</span>
        </div>
        <div class="grid-container">
            <div class="grid-x">
                <carousel :per-page-custom="[[375, 2], [768, 3], [1024, 4], [1824, 5]]" :loop="true" :autoplayLoop="true" :autoplay="false" :navigation-enabled="true" :pagination-enabled="true">
                    @foreach ($products as $product)
                        <slide>
                            <a href="{{ route('catalog.view', [$product->slug]) }}" title="{{ $product->title }}" class="item margin-bottom-1 text-center flex-container flex-dir-column">
                                {!! Html::image(route('imagecache', ['product_widget', $product->mainImage]), null, ['alt' => $product->title, 'title' => $product->title, 'width' => '165', 'height' => '240']) !!}
                                <h6 class="margin-top-1">{{ $product->title }}</h6>
                                <p>{{ $product->anonce }}</p>
                                <div class="clear clearfix"></div>
                                @if($product->discount_price)
                                    <div>
                                        <span class="old_price">
                                             {{ $product->price }} <i class="mdi mdi-currency-rub"></i>
                                        </span>
                                        <span class="discount_price">
                                            {{ $product->discount_price }} <i class="mdi mdi-currency-rub"></i>
                                        </span>
                                    </div>
                                @else
                                    <span class="price">
                                        {{ $product->price }} <i class="mdi mdi-currency-rub"></i>
                                    </span>
                                @endif
                            </a>
                        </slide>
                    @endforeach
                </carousel>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
@endif