@extends('layouts/general')

@section('content')
    <div class="grid-container margin-top-2 hide-for-small-only">
        <div class="grid-x grid-margin-x">
            <div class="cell">
                <nav role="navigation">
                    {!! Breadcrumbs::render('catalog-product', $product) !!}
                </nav>
            </div>
        </div>
    </div>
    <div class="grid-container margin-bottom-3 margin-top-3" id="catalog-view">
        <catalog-product inline-template product_json="{{ htmlspecialchars(json_encode($product), ENT_QUOTES, 'UTF-8') }}" variants_json="{{ $variants->toJson() }}">
            <div class="grid-x grid-margin-x">
                <div class="cell medium-1 small-12 hide-for-small-only">
                    <div class="margin-bottom-1" v-for="(image, i) in images">
                        <a @click="changePageCarousel(i)">
                            <img v-bind:src="'/imagecache/product_small_carousel/' + image" />
                        </a>
                    </div>
                </div>
                <div class="cell medium-7 small-12 text-center">
                    <div class="margin-bottom-2">
                        <carousel ref="carousel" :per-page="1" :loop="true" :autoplayLoop="true" :autoplay="false" :navigation-enabled="true" :pagination-enabled="true">
                            <slide v-for="image in images">
                                <div  class="product-image" >
                                    <img v-bind:src="'/imagecache/product_big/' + image"/>
                                </div>
                            </slide>
                        </carousel>
                    </div>
                    <div class="change-color margin-bottom-2" v-if="variants.length > 1">
                        <div class="current margin-bottom-1" >
                            Комплектация: <span>@{{ variant.title }}</span>
                        </div>
                        <ul>
                            <li v-for="item in variants">
                                <label v-bind:class="{ checked: variant.id == item.id }" v-bind:title="item.title">
                                    <input type="radio" v-model="variant" v-bind:value="item">
                                    <div v-bind:style="{backgroundImage: 'url(/' + item.image + ')' }"></div>
                                    <span v-bind:style="{background: item.hex}" v-show="variant.id == item.id"></span>
                                </label>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="cell medium-4 small-12">
                    <hgroup class="text-center medium-text-left">
                        <h1>{{ $product->title }}</h1>
                        <h2>{{ $product->anonce }}</h2>
                        <h2></h2>
                    </hgroup>
                    <div class="margin-bottom-1 text-center medium-text-left">
                        <span v-if="variant.discount_price">
                            <span class="old_price">
                                @{{ variant.price }} <i class="mdi mdi-currency-rub"></i>
                            </span>
                            <span class="discount_price">
                                @{{ variant.discount_price }} <i class="mdi mdi-currency-rub"></i>
                            </span>
                        </span>
                        <span v-else class="price">
                            @{{ variant.price }} <i class="mdi mdi-currency-rub"></i>
                        </span>
                    </div>
                    <div class="color margin-bottom-1 hide-for-small-only">
                        Бренд: <span>{{ $product->brand->title }}</span>
                    </div>
                    <div class="color margin-bottom-1 hide-for-small-only" v-if="variants.length > 1">
                        Комплектация: <span>@{{ variant.title }}</span>
                    </div>
                    <div class="colors" v-if="colors">
                        <div>
                            <div class="float-left">
                                <label for="size">Цвет</label>
                            </div>
                        </div>
                        <select>
                            <option v-bind:value="color" v-for="color in colors">
                                @{{ color.title }}
                            </option>
                        </select>
                    </div>
                    <div class="sizes" v-if="sizes">
                        <div>
                            <div class="float-left">
                                <label for="size">Размер (RU)</label>
                            </div>
                            <div class="float-right">
                                <a data-open="tableSizes" title="Таблица размеров">Таблица размеров</a>
                            </div>
                        </div>
                        <select>
                            <option v-bind:value="size" v-for="size in sizes">
                                @{{ size.title }}
                            </option>
                        </select>
                    </div>
                    <ul class="accordion" data-accordion>
                        <li class="accordion-item is-active" data-accordion-item>
                            <a href="#" class="accordion-title">Описание</a>
                            <div class="accordion-content" data-tab-content>
                                {!! $product->description !!}
                            </div>
                        </li>
                        @if ($page)
                            <li class="accordion-item" data-accordion-item>
                                <a href="#" class="accordion-title">{{ $page->title }}</a>
                                <div class="accordion-content" data-tab-content>
                                  {!! $page->text !!}
                                </div>
                            </li>
                        @endif
                        <li class="accordion-item" data-accordion-item>
                            <a href="#" class="accordion-title">Вопросы</a>
                            <div class="accordion-content" data-tab-content>
                                {!! App\Http\Controllers\ContactsController::widget() !!}
                            </div>
                        </li>
                    </ul>
                    <div id="share-socials" class="margin-bottom-2">
                        <div class="text-center medium-text-left font-italic title">Поделиться</div>
                        <div class="text-center medium-text-left">
                            <share></share>
                        </div>
                    </div>
                </div>
                <div class="reveal" id="tableSizes" data-reveal>
                    <h5>Размеры одежды</h5>
                    <p class="lead">Российский размер</p>
                    <button class="close-button" data-close aria-label="Close modal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </catalog-product>

        {!! App\Http\Controllers\CatalogController::widgetBuyWith($product) !!}
        {!! App\Http\Controllers\CatalogController::widgetInteresting($product) !!}

        <div class="grid-x">
            <div class="cell small-12 text-center">
                <a class="button hollow black" href="{{ route('catalog.brands.view', $product->brand->slug) }}" title="Все товары бренда">Все товары бренда</a>
            </div>
        </div>
    </div>
@endsection