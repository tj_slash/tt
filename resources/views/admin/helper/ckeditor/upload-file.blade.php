<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>File Upload</title>
    <meta name="csrf-token" content="{{ $token }}">
</head>
<body>
@php
    // Check the $_FILES array and save the file. Assign the correct path to a variable ($url).
    $url = $result['url'];

    echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url');</script>";
@endphp
</body>
</html>