@extends('layouts/general')

@section('content')
    <div class="grid-container margin-top-2 hide-for-small-only">
        <div class="grid-x grid-margin-x">
            <div class="cell">
                <nav role="navigation">
                    {!! Breadcrumbs::render('photoreports') !!}
                </nav>
            </div>
        </div>
    </div>
    <photoreports-list inline-template>
        <div>
            <div class="grid-container margin-vertical-3">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-3 small-12 hide-for-small-only">
                        @include('include.article-sidebar')
                    </div>
                    <div class="cell medium-9 small-12">
                        <div id="news-list">
                            <h1 class="text-center">Фоторепортажи</h1>

                            <div class="grid-x" v-if="last">
                                <div class="cell small-12 margin-bottom-2 text-center big-news">
                                    <img v-bind:src="last.big_image" v-bind:alt="last.title" v-bind:title="last.title" style="width:100%" />
                                    <time v-bind:datetime="last.created_at">@{{ last.date }}</time>
                                    <h3><a v-bind:href="last.url" v-bind:title="last.title">@{{ last.title }}</a></h3>
                                    <a v-bind:href="last.url" v-bind:title="last.title" class="more">Смотреть</a>
                                </div>
                            </div>

                            <div class="grid-x" v-if="photoreports.length == 0">
                                <h4 class="text-center width-100">Фоторепортажи не найдены</h4>
                            </div>

                            @if(!$photoreports->isEmpty())
                                <div class="grid-x grid-margin-x">
                                    <div class="cell large-4 medium-3 small-12 margin-bottom-2 text-center medium-text-left" v-for="item in photoreports">
                                        <img v-bind:src="item.image" v-bind:alt="item.title" v-bind:title="item.title" style="width:100%" />
                                        <time v-bind:datetime="item.created_at">@{{ item.date }}</time>
                                        <h3><a v-bind:href="item.url" v-bind:title="item.title">@{{ item.title }}</a></h3>
                                    </div>
                                </div>
                                <div class="grid-x">
                                    <div class="cell small-12 text-center">
                                        <a class="button hollow black" title="Смотреть еще" @click="showMore(this)" v-if="this.count > this.limit && this.count != this.loaded && this.count > 0">
                                            Смотреть еще
                                        </a>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </photoreports-list>
@endsection