@extends('layouts/general')

@section('content')
    <div class="grid-container margin-top-2 hide-for-small-only">
        <div class="grid-x grid-margin-x">
            <div class="cell">
                <nav role="navigation">
                    @if($photoreport)
                        {!! Breadcrumbs::render('photoreports-view', $photoreport) !!}
                    @else
                        {!! Breadcrumbs::render('photoreports') !!}
                    @endif
                </nav>
            </div>
        </div>
    </div>
    <div class="grid-container margin-vertical-3">
        <div class="grid-x grid-margin-x">
            <div class="cell medium-3 small-12 hide-for-small-only">
                @include('include.article-sidebar')
            </div>
            <div class="cell medium-9 small-12">
                <div id="news-list">
                    @if($photoreport)
                        <h1 class="text-center">{{ $photoreport->title }}</h1>
                        <div class="grid-x">
                            <div class="cell small-12 margin-bottom-2 text-center big-news">
                                {!! Html::image(route('imagecache', ['news_big', $photoreport->big_image]), null, ['alt' => $photoreport->title, 'title' => $photoreport->title, 'style' => 'width:100%']) !!}
                                <time datetime="{{ $photoreport->created_at }}">{{ Jenssegers\Date\Date::parse($photoreport->created_at)->format('j F') }}</time>
                            </div>
                            <div class="cell small-12 margin-bottom-2 big-news">
                                {!! $photoreport->text !!}
                            </div>
                            @if ($photoreport->images)
                                <photogallery :images="{{ json_encode($photoreport->imagess) }}" :small-images="{{ json_encode($smallImages) }}"></photogallery>
                            @endif
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection