<!DOCTYPE html>
<html lang="ru">
    <head>
        @if(App::environment('production'))
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126294858-1"></script>
            <script>
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());

                gtag('config', 'UA-126294858-1');
            </script>
        @endif
        @if(App::environment('production'))
            <!-- Yandex.Metrika counter -->
            <script type="text/javascript" >
                (function (d, w, c) {
                    (w[c] = w[c] || []).push(function() {
                        try {
                            w.yaCounter49801285 = new Ya.Metrika2({
                                id:49801285,
                                clickmap:true,
                                trackLinks:true,
                                accurateTrackBounce:true,
                                webvisor:true
                            });
                        } catch(e) { }
                    });

                    var n = d.getElementsByTagName("script")[0],
                        s = d.createElement("script"),
                        f = function () { n.parentNode.insertBefore(s, n); };
                    s.type = "text/javascript";
                    s.async = true;
                    s.src = "https://mc.yandex.ru/metrika/tag.js";

                    if (w.opera == "[object Opera]") {
                        d.addEventListener("DOMContentLoaded", f, false);
                    } else { f(); }
                })(document, window, "yandex_metrika_callbacks2");
            </script>
            <noscript><div><img src="https://mc.yandex.ru/watch/49801285" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
            <!-- /Yandex.Metrika counter -->
        @endif
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="yandex-verification" content="79287f405ae1f0da" />

        <title>T.T. магазин - @yield('title')</title>

        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/images/apple-touch-icon.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/images/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/images/favicon-16x16.png') }}">
        <link rel="manifest" href="{{ asset('/images/manifest.json') }}">
        <link rel="mask-icon" href="{{ asset('/images/safari-pinned-tab.svg') }}" color="#5bbad5">
        <meta name="theme-color" content="#ffffff">

        <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
        @stack('styles')
    </head>
    <body>
        <div id="app">
            @include('include.notifications')
            @include('include.header')
            <div id="content">
                @yield('content')
            </div>
            @include('include.footer')
        </div>
        @stack('scripts_before')
        <script async src="https://usocial.pro/usocial/usocial.js?v=6.1.4" data-script="usocial" charset="utf-8"></script>
        <script src="{{ asset('/js/app.js') }}"></script>
        <script src="{{ asset('/js/foundation.js') }}"></script>
        @stack('scripts_after')
    </body>
</html>
