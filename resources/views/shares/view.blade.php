@extends('layouts/general')

@section('content')
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x">
            <div class="cell">
                <nav role="navigation">
                    @if($share)
                        {!! Breadcrumbs::render('shares-view', $share) !!}
                    @else
                        {!! Breadcrumbs::render('shares') !!}
                    @endif
                </nav>
            </div>
        </div>
    </div>
    <div class="grid-container">
        <div class="grid-x grid-margin-x">
            <div class="cell small-12">
                <div id="news-list" class="margin-bottom-3 margin-top-3">
                    @if($share)
                        <h1 class="text-center">{{ $share->title }}</h1>
                        <div class="grid-x">
                            <div class="cell small-12 margin-bottom-2 text-center big-news">
                                {!! Html::image(route('imagecache', ['share_big', $share->image]), null, ['alt' => $share->title, 'title' => $share->title]) !!}
                            </div>
                            <div class="cell small-12 margin-bottom-2 big-news">
                                {!! $share->text !!}
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection