@extends('layouts/general')

@section('content')
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x">
            <div class="cell">
                <nav role="navigation">
                    {!! Breadcrumbs::render('shares') !!}
                </nav>
            </div>
        </div>
    </div>
    <article class="margin-bottom-3">
        <div class="title-line without-line">
            <span class="font-italic">Акции</span>
        </div>

        @if (!$shares->isEmpty())
            <shares-list inline-template class="shares-list">
                <div>
                    <div class="grid-container margin-top-2">
                        <div class="grid-x grid-margin-x align-center">
                            <div class="button-group">
                                <a class="button hollow black" @click="toggleSex({{\App\Models\Shares::MALE}})" :class="{active: sex === {{\App\Models\Shares::MALE}}}">Мужское</a>
                                <a class="button hollow black" @click="toggleSex({{\App\Models\Shares::FEMALE}})" :class="{active: sex === {{\App\Models\Shares::FEMALE}}}">Женское</a>
                            </div>
                        </div>
                    </div>
                    <div class="grid-container margin-top-2">
                        <div class="grid-x grid-margin-x">
                            @foreach ($shares as $i => $share)
                                <div class="cell small-12 share-item margin-bottom-2 share-sex-{{ $share->sex }} {{ ($i > 6) ? 'hide' : '' }} position-relative">
                                    <a href="{{ route('shares.view', [$share->slug]) }}" title="{{ $share->title }}">
                                        {!! Html::image(route('imagecache', ['share_big', $share->image]), null, ['alt' => $share->title, 'title' => $share->title]) !!}
                                    </a>
                                    <br/>
                                    {{ $share->title }}
                                    @if ($share->stop_date)
                                        <countdown :time="{{ $share->diff }}" v-bind:auto-start="true">
                                            <template scope="props">
                                                <span class="countdown">
                                                    До конца акции: @{{ props.days }} дней @{{ props.minutes }} мин @{{ props.seconds }} сек
                                                </span>
                                            </template>
                                        </countdown>
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="grid-x toggle-all">
                        <div class="cell small-12 text-center">
                            <a class="button hollow black" title="Все акции" @click="toggleAll(this)">Все акции</a>
                        </div>
                    </div>
                </div>
            </shares-list>
        @endif
    </article>
@endsection