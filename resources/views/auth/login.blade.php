@extends('layouts.app')

@section('content')
    <div class="grid-x grid-margin-x">
        <div class="cell small-12 medium-4 medium-offset-4 padding-vertical-3 margin-bottom-3">
            <h5 class="text-center">Авторизация</h5>
            <form method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <label for="email" class="{{ $errors->has('email') ? 'is-invalid-label' : '' }}">
                    E-Mail
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                    @if ($errors->has('email'))
                        <span class="form-error is-visible">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </label>
                <label for="password" class="{{ $errors->has('email') ? 'is-invalid-label' : '' }}">
                    Пароль
                    <input id="password" type="password" class="form-control" name="password" required>
                    @if ($errors->has('password'))
                        <span class="form-error is-visible">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </label>
                <div class="grid-x grid-margin-x">
                    <div class="cell small-12 medium-4">
                        <button type="submit" class="button">
                            Войти
                        </button>
                    </div>
                    <div class="cell small-12 medium-8 text-right">
                        <a class="clear button" href="{{ route('password.request') }}">
                            Forgot Your Password?
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
