@if (!$partners->isEmpty())
    <section id="partners" class="margin-bottom-2 vue-carousel-pagination-inner">
        <div class="title-line margin-bottom-1">
            <span>Наши партнёры</span>
        </div>
        <div class="grid-container">
            <div class="grid-x">
                <carousel :per-page-custom="[[320, 1], [768, 3], [1024, 3], [1824, 3]]" :loop="true"
                          :autoplayLoop="true" :autoplay="true" :navigation-enabled="false"
                          :pagination-enabled="true">
                    @foreach ($partners as $partner)
                        <slide style="flex-basis: auto">
                            <a class="partner" href="{{ $partner->url }}" target="_blank" title="{{ $partner->title }}">
                                <img src="{{ $partner->image }}" alt="{{ $partner->title }}" title="{{ $partner->title }}" />
                                <span>Подробнее</span>
                            </a>
                        </slide>
                    @endforeach
                </carousel>
            </div>
        </div>
    </section>
@endif