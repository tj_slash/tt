    <section id="banners" class="margin-bottom-2">
        <div class="grid-container">
            <div class="grid-x grid-margin-x">
                <div class="cell medium-6 small-12 margin-bottom-2">
                    <a class="floor" href="{{ $banners[0]->url }}">
                        <img src="{{ $banners[0]->image }}" style="width: 100%"/>
                    </a>
                </div>
                <div class="cell medium-3 small-12 margin-bottom-2">
                    <a class="floor" href="{{ $banners[1]->url }}">
                        <img src="{{ $banners[1]->image }}" style="width: 100%"/>
                    </a>
                </div>
                <div class="cell medium-3 small-12 margin-bottom-2">
                    <a class="floor" href="{{ $banners[2]->url }}">
                        <img src="{{ $banners[2]->image }}" alt="{{ $banners[2]->title }}" title="{{ $banners[2]->title }}"  style="width: 100%"/>
                    </a>
                </div>
            </div>
            <div class="grid-x grid-margin-x">
                <div class="cell medium-6 small-12 margin-bottom-2 hide-for-small-only">
                    {!! App\Http\Controllers\SubscribesController::widget() !!}
                </div>
                <div class="cell medium-3 small-12 margin-bottom-2">
                    <a class="floor" href="{{ $banners[3]->url }}">
                        <img src="{{ $banners[3]->image }}"  style="width: 100%"/>
                    </a>
                </div>
                <div class="cell medium-3 small-12 margin-bottom-2">
                    <a class="floor" href="{{ $banners[4]->url }}">
                        <img src="{{ $banners[4]->image }}" style="width: 100%" />
                    </a>
                </div>
            </div>
        </div>
    </section>