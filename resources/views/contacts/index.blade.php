@extends('layouts/general')

@section('content')
    <div class="grid-container margin-top-2 hide-for-small-only">
        <div class="grid-x grid-margin-x">
            <div class="cell">
                <nav role="navigation">
                    {!! Breadcrumbs::render('contacs') !!}
                </nav>
            </div>
        </div>
    </div>
    <div class="grid-container">
        <div class="grid-x grid-margin-x">
            <div class="cell small-12">
                <article class="margin-bottom-1 margin-top-3">
                    <h1 class="text-center">Наши контакты</h1>
                </article>
            </div>
        </div>
    </div>
    <div class="text-center hide-for-small-only map-on-contacts">
        <script charset="utf-8" src="https://widgets.2gis.com/js/DGWidgetLoader.js"></script>
        <script charset="utf-8">new DGWidgetLoader({"width":1100,"height":460,"borderColor":"#a3a3a3","pos":{"lat":50.2637503711435,"lon":127.53075599670412,"zoom":16},"opt":{"city":"blagoveshensk"},"org":[{"id":"70000001025973651"}]});</script>
    </div>
    <div class="grid-container">
        <div class="grid-x grid-margin-x">
            <div class="cell small-12">
                <article class="margin-bottom-3 margin-top-1">
                    <div class="grid-x margin-top-2">
                        <div class="cell small-12 medium-3">
                            <ul class="margin-0">
                                <li class="display-block"><i class="mdi mdi-map-marker"></i> г. Благовещенск, ул. Амурская, 170</li>
                                <li class="display-block"><i class="mdi mdi-link"></i> <a href="http://tt-store.ru" target="_blank" class="black">www.tt-store.ru</a></li>
                            </ul>
                        </div>
                        <div class="cell small-12 medium-3">
                            <ul>
                                <li class="display-block">
                                    <a href="tel:+74162443000" target="_blank" class="black">
                                        <i class="mdi mdi-phone"></i> +7 (4162) 44-30-00
                                    </a>
                                </li>
                                <li class="display-block">
                                    <a href="tel:+74162443030" target="_blank" class="black">
                                        <i class="mdi mdi-phone"></i> +7 (4162) 44-30-30
                                    </a>
                                </li>
                                <li class="display-block">
                                    <a href="tel:+74162445050" target="_blank" class="black">
                                        <i class="mdi mdi-phone"></i> +7 (4162) 44-50-50
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="cell small-12 medium-3">
                            <ul>
                                <li class="display-block"><i class="mdi mdi-clock"></i> Ежедневно: 10.00 - 19.00</li>
                            </ul>
                        </div>
                        <div class="cell small-12 medium-3">
                            <ul>
                                <li class="display-block"><i class="mdi mdi-mail-ru"></i> <a href="mailto:info@tt-store.ru" target="_blank" class="black">info@tt-store.ru</a></li>
                                <li class="display-block"> <a href="https://instagram.com/tt_store_blg" target="_blank" class="black"><i class="mdi mdi-instagram"></i> tt_store_blg</a></li>
                            </ul>
                        </div>
                    </div>
                </article>
                <div class="grid-x grid-padding-x align-center">
                    <div class="cell large-4 medium-6 small-12">
                        {!! App\Http\Controllers\ContactsController::widget() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection