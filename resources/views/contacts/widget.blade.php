@push('scripts_after')
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endpush
{{ Form::open(['url' => route('contacts.send'), 'id' => 'feedbacks-form']) }}
    <h5>Напишите нам</h5>
    <div class="input-group">
        {!! Form::text('name', old('name'), ['placeholder' => 'Ваше имя', 'class' => 'input-group-field', 'required' => 'required']) !!}
    </div>
    <div class="input-group">
        {!! Form::email('email', old('email'), ['placeholder' => 'Ваш e-mail', 'class' => 'input-group-field', 'required' => 'required']) !!}
    </div>
    <div class="input-group">
        {!! Form::textarea('text', old('text'), ['placeholder' => 'Сообщение', 'class' => 'input-group-field', 'required' => 'required', 'rows' => 4]) !!}
    </div>
    <div class="input-group">
        <div class="g-recaptcha" data-sitekey="{{config('recaptcha.public_key')}}"></div>
    </div>
    <div class="input-group align-center">
        {!! Form::submit('Отправить', ['class' => 'button black']) !!}
    </div>
    <p>
        Нажимая кнопку &laquo;Отправить&raquo;, вы соглашаетесь на обработку персональных данных в соответствии с <a href="{{ route('pages.view', 'privacy-policy') }}" target="_blank">Политикой конфиденциальности</a>.
    </p>
{!! Form::close() !!}