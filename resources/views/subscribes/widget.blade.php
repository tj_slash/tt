<div class="callout black padding-left-3 padding-right-3" style="height:180px">
    {{ Form::open(['url' => route('subscribes.send'), 'class' => 'white']) }}
        <h5>Хочу узнавать о новинках</h5>
        <div class="input-group">
        	{!! Form::email('email', old('email'), ['placeholder' => 'Введите Ваш электронный адрес', 'class' => 'input-group-field', 'required' => 'required']) !!}
            <div class="input-group-button">
        		{!! Form::submit('Отправить', ['class' => 'button black']) !!}
            </div>
        </div>
        <fieldset>
        	{!! Form::radio('sex', 2, true, ['id' => 'female', 'required' => 'required']) !!}
        	{!! Form::label('female', 'Милым дамам') !!}
        	{!! Form::radio('sex', 1, false, ['id' => 'male', 'required' => 'required']) !!}
        	{!! Form::label('male', 'Кавалерам') !!}
        </fieldset>
    {!! Form::close() !!}
</div>