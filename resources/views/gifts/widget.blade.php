<div class="callout white padding-left-3 padding-right-3">
    {{ Form::open(['url' => route('gifts.send')]) }}
        <h5>Дарим Вам скидку!</h5>
        <div class="input-group">
            {!! Form::email('email', old('email'), ['placeholder' => 'Введите Ваш электронный адрес', 'class' => 'input-group-field', 'required' => 'required']) !!}
            <div class="input-group-button">
                {!! Form::submit('Отправить', ['class' => 'button black']) !!}
            </div>
        </div>
        <fieldset>
            {!! Form::radio('sex', 2, true, ['id' => 'gifts-female', 'required' => 'required']) !!}
            {!! Form::label('gifts-female', 'Милым дамам') !!}
            {!! Form::radio('sex', 1, false, ['id' => 'gifts-male', 'required' => 'required']) !!}
            {!! Form::label('gifts-male', 'Кавалерам') !!}
        </fieldset>
    {!! Form::close() !!}
</div>