@extends('layouts/general')

@section('content')
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x">
            <div class="cell">
                <nav role="navigation">
                    {!! Breadcrumbs::render('brands') !!}
                </nav>
            </div>
        </div>
    </div>
    @if (!$brands->isEmpty())
        <section id="brands" class="margin-bottom-2 white">
            <article>
                <h1 class="text-center margin-bottom-2">Наши бренды</h1>
                <div class="grid-container">
                    <div class="grid-x grid-margin-x">
                        @foreach ($brands as $brand)
                            <div class="cell medium-2 small-12 brand-wrapper">
                                <a class="brand" href="{{ route('catalog.brands.view', [$brand->slug]) }}" title="{{ $brand->title }}">
                                    {!! Html::image(route('imagecache', ['170x90', $brand->image]), null, ['alt' => $brand->title, 'title' => $brand->title]) !!}
                                </a>
                                @if($brand->link)
                                    <a class="brand-site-link brand-site-link__black" href="{{ $brand->link }}" title="{{ $brand->title }}">Перейти на сайт</a>
                                @endif
                            </div>
                        @endforeach
                    </div>
                </div>
            </article>
        </section>
    @endif
@endsection