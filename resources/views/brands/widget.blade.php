@if (!$brands->isEmpty())
    <section id="brands" class="margin-bottom-2">
        <div class="grid-container">
            <div class="grid-x grid-margin-x">
                @foreach ($brands->take(8) as $brand)
                    <div class="cell medium-2 small-12 brand-wrapper">
                        <a class="brand" href="{{ route('catalog.brands.view', [$brand->slug]) }}" title="{{ $brand->title }}">
                            {!! Html::image(route('imagecache', ['170x90', $brand->image]), null, ['alt' => $brand->title, 'title' => $brand->title]) !!}
                        </a>
                        @if($brand->link)
                            <a class="brand-site-link" href="{{ $brand->link }}" title="{{ $brand->title }}">Перейти на сайт</a>
                        @endif
                    </div>
                @endforeach

                <div class="cell medium-4 small-12">
                    <div class="total text-center">
                        <h5>Мы работаем с ведущими мировыми брендами</h5>
                        <a href="{{ route('brands') }}" title="Бренды">Смотреть все бренды</a>
                    </div>
                </div>

                @foreach ($brands->take(-8) as $brand)
                    <div class="cell medium-2 small-12 brand-wrapper">
                        <a class="brand" href="{{ route('catalog.brands.view', [$brand->slug]) }}" title="{{ $brand->title }}">
                            {!! Html::image(route('imagecache', ['170x90', $brand->image]), null, ['alt' => $brand->title, 'title' => $brand->title]) !!}
                        </a>
                        @if($brand->link)
                            <a class="brand-site-link" href="{{ $brand->link }}" title="{{ $brand->title }}">Перейти на сайт</a>
                        @endif
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endif