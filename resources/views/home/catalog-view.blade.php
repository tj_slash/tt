@extends('layouts/general')

@section('content')
    <div class="grid-container margin-bottom-3 margin-top-3" id="catalog-view">
        <div class="grid-x grid-margin-x">
            <div class="cell medium-1 small-12">

            </div>
            <div class="cell medium-7 small-12 text-center">
                <carousel :per-page-custom="[[320, 1],[1824, 1]]" :loop="true" :autoplayLoop="true" :autoplay="false" :navigation-enabled="true" :pagination-enabled="false">
                    @for ($i = 1; $i <= 3; $i++)
                        <slide>
                            <img src="{{ $faker->imageUrl(480, 600, 'fashion') }}" alt="" title="" />
                        </slide>
                    @endfor
                </carousel>
            </div>
            <div class="cell medium-4 small-12">
                <hgroup>
                    <h1>Philipp Plein</h1>
                    <h2>Вязаное мини-платье с декоративной спинкой</h2>
                </hgroup>
                <div class="price">87 250 &#x20bd;</div>
                <dl>
                    <dt>Цвет:</dt>
                    <dd>Черный</dd>
                </dl>
                <div class="sizes">
                    <div>
                        <div class="float-left">
                            <label for="size">Размер (Ru)</label>
                        </div>
                        <div class="float-right">
                            <a href="" title="Таблица размеров">Таблица размеров</a>
                        </div>
                    </div>
                    <select id="size" name="size">
                        <option>Выберите размер</option>
                    </select>
                </div>
                <ul class="accordion" data-accordion>
                    <li class="accordion-item is-active" data-accordion-item>
                        <a href="#" class="accordion-title">Описание</a>
                        <div class="accordion-content" data-tab-content>
                          <p>Модель Romanye из осенне-зимней коллекции 2017 ‒ новый взгляд на маленькое черное платье. Здесь оно напоминает облегающий топ, надетый с расклешенной мини-юбкой. Это деление подчеркнуто фактурным узором: лиф — в вертикальную полоску, подол — горизонтальную. Филипп Плейн выбрал для создания модели прочную вискозную пряжу. Спинка украшена ажурной вставкой, в которой угадывается череп.</p>
                        </div>
                    </li>
                    {{--<li class="accordion-item" data-accordion-item>--}}
                        {{--<a href="#" class="accordion-title">Доставка и возврат</a>--}}
                        {{--<div class="accordion-content" data-tab-content>--}}
                          {{--<p>Модель Romanye из осенне-зимней коллекции 2017 ‒ новый взгляд на маленькое черное платье. Здесь оно напоминает облегающий топ, надетый с расклешенной мини-юбкой. Это деление подчеркнуто фактурным узором: лиф — в вертикальную полоску, подол — горизонтальную. Филипп Плейн выбрал для создания модели прочную вискозную пряжу. Спинка украшена ажурной вставкой, в которой угадывается череп.</p>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                    <li class="accordion-item" data-accordion-item>
                        <a href="#" class="accordion-title">Вопросы</a>
                        <div class="accordion-content" data-tab-content>
                          <p>Модель Romanye из осенне-зимней коллекции 2017 ‒ новый взгляд на маленькое черное платье. Здесь оно напоминает облегающий топ, надетый с расклешенной мини-юбкой. Это деление подчеркнуто фактурным узором: лиф — в вертикальную полоску, подол — горизонтальную. Филипп Плейн выбрал для создания модели прочную вискозную пряжу. Спинка украшена ажурной вставкой, в которой угадывается череп.</p>
                        </div>
                    </li>
                </ul>
                <div id="share-socials">
                    <div class="text-left font-italic">Поделиться</div>
                    <div>
                        <a href="" target="_blank" title=""><img src="/images/facebook-letter-logo.svg"></a>
                        <a href="" target="_blank" title=""><img src="/images/twitter-logo-silhouette.svg"></a>
                        <a href="" target="_blank" title=""><img src="/images/vk-social-network-logo.svg"></a>
                        <a href="" target="_blank" title=""><img src="/images/pinterest-logo.svg"></a>
                        <a href="" target="_blank" title=""><img src="/images/email.svg"></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid-x grid-margin-x">
            <section id="novelties">
                <div class="font-italic text-center">С этим товаром часто смотрят</div>
                <div class="grid-container">
                    <div class="grid-x padding-left-2 padding-right-2">
                        <carousel :per-page-custom="[[320, 1], [768, 3], [1024, 4], [1824, 5]]" :loop="true" :autoplayLoop="true" :autoplay="false" :navigation-enabled="true" :pagination-enabled="false">
                            @for ($i = 1; $i <= 10; $i++)
                                <slide>
                                    <a href="" title="Куртка стеганая на синтепоне" class="item">
                                        <img src="{{ $faker->imageUrl(165, 240, 'fashion') }}" alt="" title="" class="margin-bottom-1" />
                                        <h6>T BY ALEXANDER WANG</h6>
                                        <p>Куртка стеганая на синтепоне</p>
                                    </a>
                                </slide>
                            @endfor
                        </carousel>
                    </div>
                </div>
            </section>
        </div>

        <div class="grid-x grid-margin-x">
            <section id="novelties">
                <div class="font-italic text-center">Интересные предложения для Вас</div>
                <div class="grid-container">
                    <div class="grid-x padding-left-2 padding-right-2">
                        <carousel :per-page-custom="[[320, 1], [768, 3], [1024, 4], [1824, 5]]" :loop="true" :autoplayLoop="true" :autoplay="false" :navigation-enabled="true" :pagination-enabled="false">
                            @for ($i = 1; $i <= 10; $i++)
                                <slide>
                                    <a href="" title="Куртка стеганая на синтепоне" class="item">
                                        <img src="{{ $faker->imageUrl(165, 240, 'fashion') }}" alt="" title="" class="margin-bottom-1" />
                                        <h6>T BY ALEXANDER WANG</h6>
                                        <p>Куртка стеганая на синтепоне</p>
                                    </a>
                                </slide>
                            @endfor
                        </carousel>
                    </div>
                </div>
            </section>
        </div>

        <div class="grid-x">
            <div class="cell small-12 text-center">
                <a class="button hollow black" href="/news" title="Все товары бренда">Все товары бренда</a>
            </div>
        </div>
    </div>
@endsection