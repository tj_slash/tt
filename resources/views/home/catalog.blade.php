@extends('layouts/general')

@section('content')

    <div class="grid-container margin-bottom-3 margin-top-3" id="catalog">
        <h1 class="text-center">Каталог</h1>

        <div class="grid-x grid-margin-x">
            <div class="cell medium-offset-2 medium-8 small-12 text-center">
                80 000 товаров
            </div>
            <div class="cell medium-2 small-12">
                <select>
                    <option value="">На ваш вкус</option>
                    <option value="">По новизне</option>
                    <option value="">По популярности</option>
                    <option value="">По цене -</option>
                    <option value="">По цене +</option>
                </select>
            </div>
        </div>

        <div class="grid-x grid-margin-x">
            <div class="cell medium-3 small-12 padding-top-3">
                {!! App\Http\Controllers\CatalogController::sidebar() !!}
            </div>
            <div class="cell medium-9 small-12">
                <div class="grid-x">
                    @for ($i = 1; $i <= 12; $i++)
                        <div class="cell large-4 medium-4 small-12 item text-center margin-bottom-2 padding-horizontal-3 padding-vertical-1">
                            <a href="/catalog/view" title="">
                                <img src="/images/item.jpg" alt="Givenchy" title="Givenchy" />
                                <h3>Givenchy</h3>
                                <span>Маска для лица Le Soin Noir &amp; Blanc Masque</span>
                                <span>19 130 &#x20bd;</span>
                            </a>
                        </div>
                    @endfor
                </div>
                <div class="grid-x">
                    <div class="cell small-12 text-center">
                        <a class="button hollow black" href="/news" title="Смотреть еще">Смотреть еще</a>
                    </div>
                </div>
                <ul class="pagination text-center margin-top-2">
                    <li class="disabled">&#9666;</li>
                    <li class="current">1</li>
                    <li><a href="#" aria-label="Page 2">2</a></li>
                    <li><a href="#" aria-label="Page 3">3</a></li>
                    <li><a href="#" aria-label="Page 4">4</a></li>
                    <li class="ellipsis"></li>
                    <li><a href="#" aria-label="Page 12">12</a></li>
                    <li><a href="#" aria-label="Page 13">13</a></li>
                    <li><a href="#">&#9656;</a></li>
                </ul>
            </div>
        </div>
    </div>
@endsection