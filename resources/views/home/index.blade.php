@extends('layouts/general')

@section('content')
    {!! App\Http\Controllers\SlidersController::widget() !!}
    {!! App\Http\Controllers\CatalogController::widgetNew() !!}
    {!! App\Http\Controllers\BannersController::widget() !!}
    {!! App\Http\Controllers\CatalogController::widgetRecommend() !!}
    {!! App\Http\Controllers\NewsController::widget() !!}

    <section id="share-socials" class="margin-bottom-2">
        <div class="title-line without-line">
            <span class="font-italic">Поделиться</span>
        </div>
        <div class="text-center">
            <share></share>
        </div>
    </section>

    <section id="our-floors" class="margin-bottom-2 vue-carousel-pagination-inner">
        <div class="title-line">
            <span>Наш TT-STORE</span>
        </div>
        <div class="grid-container">
            <div class="grid-x grid-margin-x hide-for-small-only">
                <div class="cell medium-4 small-12">
                    <a class="floor" href="/floors">
                        <div>1</div>
                        <p>Этаж</p>
                    </a>
                </div>
                <div class="cell medium-4 small-12">
                    <a class="floor" href="/floors">
                        <div>2</div>
                        <p>Этаж</p>
                    </a>
                </div>
                <div class="cell medium-4 small-12">
                    <a class="floor" href="/floors">
                        <div>3</div>
                        <p>Этаж</p>
                    </a>
                </div>
            </div>
        </div>
        <div class="show-for-small-only">
            <carousel :per-page-custom="[[360, 1]]" :loop="true" :autoplayLoop="true" :autoplay="false" :navigation-enabled="false" :pagination-enabled="true">
                <slide>
                    <a class="floor" href="/floors">
                        <div>1</div>
                        <p>Этаж</p>
                    </a>
                </slide>
                <slide>
                    <a class="floor" href="/floors">
                        <div>2</div>
                        <p>Этаж</p>
                    </a>
                </slide>
                <slide>
                    <a class="floor" href="/floors">
                        <div>3</div>
                        <p>Этаж</p>
                    </a>
                </slide>
            </carousel>
        </div>
    </section>
    <div class="clearfix"></div>
    <div class="hide-for-small-only">
        {!! App\Http\Controllers\BrandsController::widget() !!}
    </div>
    <div class="clearfix"></div>
    {!! App\Http\Controllers\MainController::instagram() !!}
    {!! App\Http\Controllers\PartnersController::widget() !!}
@endsection