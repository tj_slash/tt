@extends('layouts/general')

@section('content')
    <div class="grid-container margin-top-2 hide-for-small-only">
        <div class="grid-x grid-margin-x">
            <div class="cell">
                <nav role="navigation">
                    {!! Breadcrumbs::render('floors') !!}
                </nav>
            </div>
        </div>
    </div>
    <section id="our-floors" class="margin-bottom-3 vue-carousel-pagination-inner">
        <div class="title-line without-line">
            <span class="font-italic">Этажи</span>
        </div>
        @if (!$floors->isEmpty())
            <div class="grid-container margin-top-2">
                <div class="grid-x grid-margin-x">
                    @foreach ($floors as $floor => $brands)
                        <div class="cell medium-4 small-12">
                            <a class="floor margin-bottom-1" href="">
                                <div>{{ ($floor+1) }}</div>
                                <p>Этаж</p>
                            </a>
                            @foreach ($brands as $brand)
                                <a href="{{ route('catalog.brands.view', [$brand->slug]) }}" title="{{ $brand->title }}">
                                    {{ $brand->title }}
                                </a>
                                @if (!$loop->last)
                                    ,
                                @endif
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
    </section>
    <div class="clearfix"></div>
@endsection