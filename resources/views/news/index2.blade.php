@extends('layouts/general')

@section('content')
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x">
            <div class="cell">
                <nav role="navigation">
                    {!! Breadcrumbs::render('news') !!}
                </nav>
            </div>
        </div>
    </div>
    <div class="grid-container margin-vertical-3">
        <div class="grid-x grid-margin-x">
            <div class="cell medium-3 small-12">
                @include('include.article-sidebar')
            </div>
            <div class="cell medium-9 small-12">
                <div id="news-list">
                    <h1 class="text-center">Новости и статьи</h1>

                    @if($lastNews)
                        <div class="grid-x">
                            <div class="cell small-12 margin-bottom-2 text-center big-news">
                                {!! Html::image(route('imagecache', ['news_big', $lastNews->big_image]), null, ['alt' => $lastNews->title, 'title' => $lastNews->title]) !!}
                                <time datetime="{{ $lastNews->created_at }}">{{ Carbon\Carbon::parse($lastNews->created_at)->formatLocalized('%d %B') }}</time>
                                <h3><a href="{{ route('news.view', [$lastNews->slug]) }}" title="{{ $lastNews->title }}">{{ $lastNews->title }}</a></h3>
                                <a href="{{ route('news.view', [$lastNews->slug]) }}" title="{{ $lastNews->title }}" class="more">Читать новость</a>
                            </div>
                        </div>
                    @endif

                    @if(!$news->isEmpty())
                        <news-list inline-template>
                            <div>
                                <div class="grid-x grid-margin-x">
                                    @foreach ($news as $item)
                                        <div class="cell large-4 medium-3 small-12 margin-bottom-2 text-left">
                                            {!! Html::image(route('imagecache', ['medium', $item->image]), null, ['alt' => $item->title, 'title' => $item->title]) !!}
                                            <time datetime="{{ $item->created_at }}">{{ Carbon\Carbon::parse($item->created_at)->formatLocalized('%d %B') }}</time>
                                            <h3><a href="{{ route('news.view', [$item->slug]) }}" title="{{ $item->title }}">{{ $item->title }}</a></h3>
                                        </div>
                                    @endforeach
                                    <div class="cell large-4 medium-3 small-12 margin-bottom-2 text-left" v-for="item in news">
                                        <img v-bind:src="@{{ item.image }}" v-bind:title="@{{ item.title }}" v-bind:alt="@{{ item.title }}" />
                                        <time v-bind:datetime="@{{ item.created_at }}">@{{ item.created_at }}</time>
                                        <h3><a v-bind:href="@{{ item.slug }}" v-bind:title="@{{ item.title }}">@{{ item.title }}</a></h3>
                                    </div>
                                </div>
                                <div class="grid-x">
                                    <div class="cell small-12 text-center">
                                        <a class="button hollow black" href="/news" title="Смотреть еще" @click="showMore(this)">
                                            Смотреть еще
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </news-list>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection