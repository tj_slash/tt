@extends('layouts/general')

@section('content')
    <div class="grid-container margin-top-2 hide-for-small-only">
        <div class="grid-x grid-margin-x">
            <div class="cell">
                <nav role="navigation">
                    @if($news)
                        {!! Breadcrumbs::render('news-view', $news) !!}
                    @else
                        {!! Breadcrumbs::render('news') !!}
                    @endif
                </nav>
            </div>
        </div>
    </div>
    <div class="grid-container margin-vertical-3">
        <div class="grid-x grid-margin-x">
            <div class="cell medium-3 small-12 hide-for-small-only">
                @include('include.article-sidebar')
            </div>
            <div class="cell medium-9 small-12">
                <div id="news-list">
                    @if($news)
                        <h1 class="text-center">{{ $news->title }}</h1>
                        <div class="grid-x">
                            <div class="cell small-12 margin-bottom-2 text-center big-news">
                                {!! Html::image(route('imagecache', ['news_big', $news->image]), null, ['alt' => $news->title, 'title' => $news->title, 'style' => 'width:100%']) !!}
                                <time datetime="{{ $news->created_at }}">{{ Jenssegers\Date\Date::parse($news->created_at)->format('j F') }}</time>
                            </div>
                            <div class="cell small-12 margin-bottom-2 big-news">
                                {!! $news->text !!}
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection