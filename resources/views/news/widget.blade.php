@if($news)
    <section id="last-news" class="margin-bottom-2">
        <div class="grid-container">
            <div class="grid-x last-news-box">
                <div class="cell medium-8 small-12">
                    {!! Html::image(route('imagecache', ['news_big', $news->big_image]), null, ['alt' => $news->title, 'title' => $news->title]) !!}
                </div>
                <div class="cell medium-4 small-12">
                    <article>
                        <div>
                            <h4>{{ $news->title }}</h4>
                            <p>
                                <a href="" title="{{ $news->title }}">
                                    {!!  str_limit(strip_tags($news->text), 200) !!}
                                </a>
                            </p>
                            <a class="button hollow" href="{{ route('news') }}" title="Новости и статьи">Все материалы</a>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </section>
@endif