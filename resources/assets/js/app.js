
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Vue from 'vue';
import VueCarousel from 'vue-carousel';
// import Foundation from 'foundation';
Vue.use(VueCarousel);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('carousel-component', require('./components/CarouselComponent.vue'));
Vue.component('shares-list', require('./components/shares/list.vue'));
Vue.component('news-list', require('./components/news/list.vue'));
Vue.component('photoreports-list', require('./components/photoreports/list.vue'));
Vue.component('photogallery', require('./components/photoreports/photogallery.vue'));
Vue.component('catalog-search', require('./components/catalog/search.vue'));
Vue.component('catalog-product', require('./components/catalog/product.vue'));
Vue.component('notifications', require('./components/index/notifications.vue'));
Vue.component('look', require('./components/look/look.vue'));
Vue.component('input-clear', require('./components/input-clear/input-clear.vue'));
Vue.component('top', require('./components/index/top.vue'));
Vue.component('share', require('./components/index/share.vue'));

const app = new Vue({
    el: '#app'
});
