$(document).foundation();
if (Foundation.MediaQuery.is('small only')) {
    $('#mobile-sticky-header').addClass('mobile-sticky-header')
}
$(window).on('changed.zf.mediaquery', function(event, newSize, oldSize) {
    var stickyHeader = $('#mobile-sticky-header');
    if (newSize !== 'small') {
        stickyHeader.removeClass('mobile-sticky-header')
    } else {
        stickyHeader.addClass('mobile-sticky-header')
    }
});
$(function() {
    $('#scroll-up').click(function() {
        $('#scroll').scrollTop($('#scroll').scrollTop() - 120 );
    });
    $('#scroll-down').click(function() {
        $('#scroll').scrollTop($('#scroll').scrollTop() + 120 );
    });
});
